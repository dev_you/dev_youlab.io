---
title: HTTPS总结
date: 2016-08-01
tags: ["HTTPS"]
---

在使用AFNetworking的过程中，遇到一些HTTPS相关的问题，阅读一些资料后做下总结。

<!--more-->

## HTTP存在的问题

[W3C](https://zh.wikipedia.org/wiki/万维网联盟)和[IETF](https://zh.wikipedia.org/wiki/互联网工程任务组)设计HTTP协议的时候没有考虑安全问题，导致使用HTTP协议通信时会出现以下三个问题：

1. 窃听
2. 伪装
3. 中间人攻击

### 窃听

窃听相同IP段上的通信并非难事，只需要收集在互联网上流动的数据包就行。抓包工具(Packet Capture)或嗅探器(Sniffer)可以解析收集的数据包。

常用的抓包工具Wireshark、Charles、Fiddler，还有Linux下的Aircrack-ng破解Wi-Fi之前也必须先进行抓包；

![00-窃听](media/15540975376942/00-%E7%AA%83%E5%90%AC.png)



### 伪装

HTTP协议中的请求和响应都不会对通信双方进行身份确认，所以客户端和服务器都可能是伪装过的，无法确定正在通信的对方是否具有访问权限。

![01-伪装](media/15540975376942/01-%E4%BC%AA%E8%A3%85.png)


### 中间人攻击

HTTP协议无法证明通信的完整性，获取到的数据可能是被精心篡改过的。

![02-中间人攻击](media/15540975376942/02-%E4%B8%AD%E9%97%B4%E4%BA%BA%E6%94%BB%E5%87%BB.png)


## HTTPS = HTTP + SSL/TLS

### HTTPS的结构

HTTPS在HTTP的基础上增加了SSL/TLS，弥补了HTTP存在的3个安全缺陷。HTTP和HTTPS的结构如下：

![03-HTTP and HTTPS](media/15540975376942/03-HTTP%20and%20HTTPS.jpg)


### SSL/TLS

1. SSL/TLS为互联网通信提供安全及数据完整性保障，SSL是TLS的前身；
2. 1994年，NetScape设计了SSL协议，未发布；
3. 1995年，NetScape发布了SSL2.0版本，有严重安全漏洞；
4. 1996年，SSL3.0问世，得到大量应用；
5. 1999年，IETF接手SSL，发布了SSL的升级版TLS；
6. 2014年，Google发布SSL3.0中的设计缺陷，建议禁用此协议；
7. TLS1.0 ≈ SSL3.0、TLS1.1 ≈ SSL3.1、TLS1.2 ≈ SSL3.2；


## 解决HTTP的问题

1. 加密
2. 数字证书认证
3. 完整性校验(数字签名)

### 加密

对通信内容使用加密算法和秘钥进行加密，然后用加密后的内容进行通信，这样即使通信内容被窃听，没有秘钥(及时加密算法公开)，就无法解密。

1. 对称秘钥加密技术
	* 加密、解密使用同一个密钥；
	* 加密、解密速度快；
	* 如何将同一个密钥交给通信双方是个问题；
	* DES、3-DES、AES、RC5、RC6；
	
2. 非对称(公开)密钥加密技术
	* 加密、解密不使用同一个密钥，分公钥、私钥；
	* 公钥公开，私钥自己保存；
	* 公钥可以解密私钥加密的内容，私钥可以解密公钥加密的内容；
	* 加密、解密速度慢；
	* RSA；

![04-公钥私钥](media/15540975376942/04-%E5%85%AC%E9%92%A5%E7%A7%81%E9%92%A5.png)


3. HTTPS使用混合加密机制
	
	先用非对称密钥加密技术建立通信，间接交换对称密钥，然后用对称密钥加密。下面有详细介绍。

### 数字签名

利用数字签名就可以对通信内容进行完整请校验。

数字证书也用到了数字签名，所以先说数字签名。看图：

![05-数字签名](media/15540975376942/05-%E6%95%B0%E5%AD%97%E7%AD%BE%E5%90%8D.png)



### 数字证书

先看数字证书的构成：

![06-数字签名格式](media/15540975376942/06-%E6%95%B0%E5%AD%97%E7%AD%BE%E5%90%8D%E6%A0%BC%E5%BC%8F.jpg)


擦，写了一堆还是描述不清楚，看图：

![07-签发数字证书](media/15540975376942/07-%E7%AD%BE%E5%8F%91%E6%95%B0%E5%AD%97%E8%AF%81%E4%B9%A6.png)



## HTTPS通信流程

So, At last. HTTPS的完整通信流程

![08-HTTPS通信流程](media/15540975376942/08-HTTPS%E9%80%9A%E4%BF%A1%E6%B5%81%E7%A8%8B.png)


## 项目采用的HTTPS

* 开发、QA、预发布环境使用的是[WoSign](https://www.wosign.com)签名的证书
* 正式环境使用的是[StartCom](https://www.startssl.com)签名的证书
* 这两个证书认证机构都受Apple信任([iOS 9 中可用的受信任根证书列表](https://support.apple.com/zh-cn/HT205205))；

## AFNetworking2.4.1的问题

* 使用系统类NSURLSessionTask没有问题，使用AFNetworking2.4.1版本有问题，使用AFNetworking2.6.1没有问题；

![10-Erro](media/15540975376942/10-Error.png)


### 原因

![11-原因](media/15540975376942/11-%E5%8E%9F%E5%9B%A0.png)


* self.validatesDomainName 在类初始化时没有设置为YES，导致采用了SecPolicyCreateBasicX509而不是SecPolicyCreateSSL；
* SecPolicyCreateBasicX509和SecPolicyCreateSSL的区别并没有找到详细的说明，表面意思上应该是是否验证服务器域名；
* AFNetworking2.6.1版本已经修复了此问题，类初始化是设置self.validatesDomainName为YES；


## HTTP/2

HTTP/1.x还有其他的缺陷，HTTP/2基于SPDY协议改进了HTTP/1.1

![12-HTTP2](media/15540975376942/12-HTTP2.png)


## 参考
* 《HTTP权威指南》
* 《图解HTTP》
* [RSA part1](http://www.ruanyifeng.com/blog/2013/06/rsa_algorithm_part_one.html)
* [RSA part2](http://www.ruanyifeng.com/blog/2013/07/rsa_algorithm_part_two.html)
* [HTTP/2](https://http2.github.io/)
* [HTTP/2 Frequently Asked Questions](https://http2.github.io/faq/#what-are-the-key-differences-to-http1x)
* [Charles 从入门到精通](http://blog.devtang.com/blog/2015/11/14/charles-introduction/)
* [HTTP2.0](media/15540978671519/HTTP2.0.pdf)





