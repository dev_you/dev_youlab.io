---
title: SRT推流协议调研
date: 2021-03-24T17:07:01+08:00
tags: [音视频]
---

SRT推流协议

<!--more-->

SRT（Secure Reliable Transport）是基于 UDP 的传输协议，保证安全性、可靠性。SRT 诞生于广电行业，广电行业音视频传播以卫星、光纤为主，非常可靠但价格昂贵，SRT 的目标是依赖公网可靠的传输音视频，互联网厂商把 SRT 协议应用在直播中，可以高效的传输音视频数据，有效对抗抖动和丢包，在延迟上也有一定优势。SRT 协议有以下几个特点：

### 高效

1. 高效的握手，SRT 使用两个 RTT （Round Trip Time） 完成握手，相比较 RTMP 需要 9 个 RTT

    1. SRT 第一次握手交换 Cookie，第二次握手协商 版本、加密方式、双向延迟量、StreamId等

    1. RTMP 基于 TCP，TCP三次握手，RTMP三次握手，还有 connection、createStream、play/publish

1. 基于 UDP 高效的传输数据



### 对抗抖动

> 抖动：一组数据包在接收端和发送端的时间差差异

音视频流经过网络传输后，帧间距和码率特性有所改变，也就引入了抖动。

![](media/01.png)

SRT 协议会恢复原有的帧间隔和码率特性，消除抖动。

![](meida/02.png)

实现方式：

1. 发送端数据包附带精准的时间戳

1. 接收端有缓冲区，收到数据后根据时间戳还原



### 对抗丢包

有两种策略对抗丢包：

1. FEC（Forward Error Correction），额外发送冗余信息，产生丢包时，根据冗余信息推测出丢失信息，会增加额外的带宽，不会增加延迟

1. ARQ（Automatic Repeat Request），遇到丢包重传，不增加带宽，会增加延迟

SRT 采用 ARQ 的策略



### 延迟稳定

发送端、接收端有缓冲区，发送方、接口方设置延迟量，设置延迟量相当于设置缓冲区大小。

![](media/03.png)

SRT 协议有 TLPD（Too Late Packet Drop）机制，可以保证发送端到接收到的延迟时稳定的。发送端、接收端知道延迟量，发送方明确未发送的数据过期了，就丢弃数据。

SRT 协议一般用于主播推流到云端，可以降低主播到云端的延迟，主播到观众的延迟效果还待观察。



### SRT 测试



1. PC端，最新版本 OBS 支持 SRT 协议，经测试无问题

1. 客户端，需要推流器支持，还未支持

1. SRT 优势在对抗弱网，还需要覆盖弱网环境进行测试



```
OBS推流地址	rtmp://domain/live/
OBS推流密钥	SRT_TEST?txSecret=xxx&txTime=605EA4A3
TS over SRT 推流地址	srt://domain:9000?streamid=#!::h=domain,r=live/SRT_TEST,txSecret=xxx,txTime=605EA4A3

播放地址		http://domain/live/SRT_TEST.flv
```



### SRT 在行业的应用

[SRT在B站的落地 - 云+社区 - 腾讯云](https://cloud.tencent.com/developer/article/1599025?from=information.detail.%E8%85%BE%E8%AE%AF%E4%BA%91%20srt)

[引入SRT推流技术，斗鱼让直播变得更流畅-品玩](https://www.pingwest.com/w/233133)

[SRT协议在电视直播中的应用](https://zhuanlan.zhihu.com/p/183788918)



厂商分享的经验

1. 更适合丢帧率有所下降

1. 流控较为简单

1. 推流器和服务器需要改造（腾讯云已支持）

1. 确认防火墙策略是否有影响



### SRT vs QUIC

QUIC 也是基于 UDP 的可靠传输，对比如下：

1. QUIC 优势

    1. 建立连接更快，0~1个 RTT

    1. 流控算法灵活，插拔式

    1. 网络切换连接不会断开

    1. 厂商支持程度高

1. SRT 优势

    1. 抗抖动

    1. 延迟稳定

    1. 可丢包

    1. 针对音视频传输有优化



## 参考

[语音视频SDK如何实现超低延迟优化？](https://mp.weixin.qq.com/s?__biz=MzUxMzcxMzE5Ng==&mid=2247488215&idx=1&sn=2e226222356ba4f4abee8e7b1567f05f&source=41#wechat_redirect)

[新一代直播传输协议SRT](https://zhuanlan.zhihu.com/p/114436399)

[SRT Alliance - Open-source SRT - Secure Reliable Transport](https://www.srtalliance.org/)

[Haivision/srt](https://github.com/Haivision/srt/blob/master/docs/why-srt-was-created.md)

[Streaming Protocols Comparison](https://restream.io/blog/streaming-protocols/)



[云直播 SRT 协议推流 - 功能实践 - 文档中心 - 腾讯云](https://cloud.tencent.com/document/product/267/53955)

