---
title: 诡异的进度条跳跃问题调查记录
date: 2021-05-18T17:34:59+08:00
tags: [音视频]
---

播放器播放过程中进度条突然跳跃？？？

<!--more-->

测试过程中发现一条比较特殊的回放链接，进度条播放到第5秒后突然跳到第14秒。回放链接是：[__http://test-tx-live-playback.xxqapp.cn/6c993cbfvodcq1500003947/f6b75bfb3701925921239223127/playlist_eof.m3u8__](http://test-tx-live-playback.xxqapp.cn/6c993cbfvodcq1500003947/f6b75bfb3701925921239223127/playlist_eof.m3u8)，效果如下：

![](media/01.gif)

### 调查过程

第一感觉是视频本身出问题了，从视频本身入手。回放链接是 m3u8 格式，m3u8 只是索引文件，把一个视频切成 N 片，索引文件保存每个切片的播放地址及相关信息。上面的回放链接一部分内容如下：

```text
#EXTM3U
#EXT-X-PLAYLIST-TYPE:VOD
#EXT-X-TARGETDURATION:60
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:0
#EXTINF:4.045,
3695237818_1713970675_1.ts?start=0&end=817611&type=mpegts&resolution=544x960
#EXTINF:1.449,
3695237818_1713970675_1.ts?start=817612&end=1106003&type=mpegts&resolution=544x960
#EXT-X-DISCONTINUITY
#EXTINF:1.549,
3695237818_1184035333_2.ts?start=0&end=509291&type=mpegts&resolution=720x1280
#EXTINF:1.607,
3695237818_1612389674_13.ts?start=7444424&end=7742027&type=mpegts&resolution=544x960
#EXT-X-ENDLIST
```

几个主要的字段含义：

- #EXTM3U，m3u8文件标识

- #EXT-X-PLAYLIST-TYPE，视频类型：点播、直播

- #EXT-X-TARGETDURATION，切片文件的最大时长

- #EXT-X-VERSION，m3u8 版本号

- #EXT-X-MEDIA-SEQUENCE，播放序列参考值，从这个切片开始播放

- #EXTINF，该切片的视频时长

- #EXT-X-DISCONTINUITY，编码格式不连续，通知播放器重置解码器，可以看到视频从540p 切换到了 720p

- #EXT-X-ENDLIST，结束标识，点播才有这个标识



视频播放进度条从第5秒跳到第14秒，问题应该出现在第二个或者第三个切片，把这两个切片下载下来之后，直接播放没有问题，且 m3u8 文件中切片时长也是正确的。所以问题不在视频本身。

又试了其他播放器，VLC、IINA也都有同样的问题，浏览器直接播放反而没问题，应该是播放器的兼容问题了。经过查看播放器源码以及相关资料后，发现播放器的进度条计算方式是：

> progress = ( current_pts - start_pts ) * time_base

这里涉及到两个概念：pts、time_base。

pts 全称是 Presentation Time Stamp，每一个音频、视频帧都有这个属性，播放器用这个时间戳来控制什么时候显示这个视频帧，以及保证音视频同步。一般是设置一个初始值，然后随着时间线性增加。另外还有一个相关的概念，DTS，Decoding Time Stamp，播放器用这个时间戳控制什么时候解码这个视频帧。

time_base 是时间的分割方式，用于精准的时间控制，这个 case 中使用的 time_base 是 {1, 90000}，也就是1秒钟被分割了90000份。常见的 time_base 规格是 {1, 24}、{1, 60}、{1, 600}。这里引申出一个问题，为什么采用偏复杂的方式来精准控制时间，而不用浮点数，原因是操作浮点数会有误差，比如采用32位的浮点数，0.123456 - 0.12345 会得到 0.00001。 在视频很长的情况下，误差会逐渐累积到不可忽略的程度。

知道视频进度的计算方式后，我们可以通过 ffprobe 工具获取到每一帧的 pts，然后一帧一帧的计算进度，首先第一个视频帧的原始数据如下：

```text
// 第一帧，start_pts = 2277540
{
    "media_type": "video",
    "stream_index": 0,
    "key_frame": 1,
    "pkt_pts": 2277540,
    "pkt_dts": 2277540,
    "pkt_pos": "564",
    "pkt_size": "22960",
    "width": 544,
    "height": 960,
    "pix_fmt": "yuvj420p",
    "pict_type": "I",
}
```

分辨率从 540p 切换到 720p 时对应的原始数据：

```text
// 切换到 720p 前一帧，current_pts = 2767590
{
    "media_type": "video",
    "stream_index": 0,
    "key_frame": 0,
    "pkt_pts": 2767590,
    "pkt_dts": 2767590,
    "pkt_pos": "1096604",
    "pkt_size": "8756",
    "width": 544,
    "height": 960,
    "pix_fmt": "yuvj420p",
    "pict_type": "P",
}
```

```text
// 切换到 720p 第一帧，current_pts = 3039480
{
    "media_type": "video",
    "stream_index": 0,
    "key_frame": 1,
    "pkt_pts": 3039480,
    "pkt_dts": 3039480,
    "pkt_pos": "1106568",
    "pkt_size": "30962",
    "width": 720,
    "height": 1280,
    "pix_fmt": "yuvj420p",
    "pict_type": "I",
},
```

可以计算出来切换前后的视频进度：

> 切换前进度值 = (2767590 - 2277540) / 90000 = 5.445

> 切换后进度值 = (3039480 - 2277540) / 90000 = 8.466

可以发现进度发生了跳跃。后续分辨率从 720p 切换到 540p，又发生了跳跃。

现在已经定位到进度跳跃的表面原因是分辨率变化，真实原因是 pts 突然增加很多，那为什么 pts 会突然变大呢。前面提到 pts 是随着时间线性增长，在切换分辨率的时候需要重置编码器，这期间没有产生视频帧，但是 pts 还在增加，所以引起了该现象。



### 解决办法

问题已经定位到了，对应有两个解决思路：

1. 从推流端入手，重置编码器时，暂停 pts 增长，这样播放就没问题了

1. 从播放端入手，不采用计算 pts 得到进度的方式，创建定时器，自己维护进度，需要处理播放缓冲、暂停、快进等事件

目前推流器使用的是第三方SDK，并没有开源，所以采用了第二种方式。



















