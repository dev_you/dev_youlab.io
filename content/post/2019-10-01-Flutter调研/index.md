
---
title: Flutter调研
date: 2019-11-15T18:53:26+08:00
tags: [Flutter]
---

初步学习下Flutter.

<!--more-->

## Dart

1. 强类型语言，支持类型推断，支持泛型；
2. 不允许反射，编译时只保留需要的代码，可减少代码体积；
3. Debug模式下，支持JIT (Just in time)，实现Hot Reload；
4. Release模式，支持AOT (Ahead of time)，提高运行效率；
5. 可以转成JavaScript。

#### isolate机制
通常的并发编程线程是通过线程抢占时间片来执行代码，线程之间通过共享内存通信，会有竟态条件和执行顺序依赖的问题，iOS中通过锁和GCD调度线程解决。
Actor是另外一种并发模型，遵循的原则是: Don’t communicate by sharing memory, share memory by communicating。单个Actor执行代码是串行的，Actor之间通过消息异步通信；

1. isolate是Dart对Actor模型的实现；
2. Isolate.spawn()或者Future创建另一个isolate；
3. isolate有独立的内存，isolate之间通过eventloop在event queue上传递消息通信；
4. event queue接收Dart内部、外部的事件：I/O、手势、绘制、Timer、isolate消息等；
5. eventloop不断循环处理event queue传过来的事件；
![-w481](media/15693822571000/15697236843117.jpg)


#### 内存分配 & 垃圾回收

Dart给对象分配内存是直接在堆上移动指针，内存增长是线性的。

Dart垃圾回收分两部分：Young Space Scavenger、Parallel Marking and Concurrent Sweeping。

**Young Space Scavenger**(多生代算法)
. ![](media/15693822571000/15694884320086.jpg)

Young Space Scavenger会阻塞线程，所以Flutter Engine在App处于空闲状态时，会通知Garbage Collector，再进行回收，保证不会影响性能；另外GC还会整理内存碎片；

App运行期间，Stateless Widget会大量的创建、销毁、重建，生命周期比较短，比较适合这样垃圾回收方式。

**Parallel Marking and Concurrent Sweeping**(mark-sweep)

对象活过一定生命周期后，mark-sweep会接管这些对象，首先会遍历这些对象哪些还在使用，进行标记，然后扫描App内存空间，没有被标记的对象将被回收。

mark-sweep发生时，Dart runtime会暂停，UI线程会被阻塞，内存也不允许修改。鉴于已经有Young Space Scavenger，这种垃圾回收不会被频繁调用。

对一个isolate垃圾回收不会影响其他isolate。

## Flutter

#### 架构
![](media/15693822571000/15693951308901.jpg)

1. Engine：Skia负责图形绘制，Text负责文字排版，Dart Runtime；
2. Foundation：基础层，上层使用它实现 UI SDK，包含Animation、Painting、Gestures；
3. Rendering 性能与原生相差不多，详见 **渲染** 一节；
4. Flutter是声明氏框架，Widget描述控件，RenderObject渲染控件，Element有这两个属性，作为中间件；Widget很轻量，state发生变化，就会重新创建Widget实例，但Element不一定会；
5. Material和Cupertino是两套设计语言，对应Android和iOS，两个平台展示有差异需要判断(比如导航栏，需要判断平台来return不同的Widget)；

#### 渲染
CPU处理渲染内容的计算，GPU进行渲染，

**iOS渲染**
![](media/15693822571000/15693966258259.png)

**Flutter渲染**
![](media/15693822571000/15693986669908.png)

Flutter界面是由Widget组成的，所有Widget组成Widget Tree，界面更新时会更新 Widget Tree，然后再更新Element Tree，最后更新 RenderObject Tree。接下来的渲染流程，Flutter 渲染在 Framework 层会有 Build、Widget Tree、Element Tree、RenderObject Tree、Layout、Paint、Composited Layer 等几个阶段。将 Layer 进行组合，生成纹理，使用 OpenGL 的接口向 GPU 提交渲染内容进行光栅化与合成，是在 Flutter 的 C++ 层，使用的是 Skia 库。包括提交到 GPU 进程后，合成计算，显示屏幕的过程和 iOS 原生基本是类似的，因此性能也差不多。

#### 编译&运行

1. iOS编译出的是BitCode，上传到AppStore后再针对根据处理器翻译成不同的二进制代码。
2. Android编译出是ByteCode，运行时虚拟机根据处理器翻译成对应的机器码，用JIT和AOT来优化执行速度；
3. Flutter Debug模式，Dart被编译成Script Snapshot，Android上使用JIT翻译成二进制代码运行，iOS使用Interpreter翻译成二进制代码运行；
4. Flutter Release模式，使用AOT编译成二进制代码；

BitCode、ByteCode、Script Snapshot都是中间代码；

iOS构建使用AOT例子
![-w630](media/15693822571000/15697270728409.jpg)


#### Hot Reload
Hot Reload可以提高开发效率，Flutter依赖于JIT实现Hot Reload，在Debug模式使用。
![](media/15693822571000/15697258507050.png)

## 混合开发
混合开发有两种形式：
![](media/15693822571000/15697263791820.jpg)
第一种耦合度高，第二种可以三端分开开发。

#### 流程
1. 创建Flutter项目 `flutter create -t moudle flutter_hybrid`；
2. 开发过程直接用Android Studio在Flutter项目上更改；
3. Flutter项目可以编译出aar、framework，分别对应Android、iOS；
4. Android可以通过Gradle依赖aar，iOS通过Pod依赖framework，与普通第三方库一致；
5. 构建流程和原生一致；

#### 机制
iOS开发依赖第三方库都是*.framework，pod install时会调用Flutter项目中的`podhelper.rb`脚本，脚本把Flutter Engine编译成Flutter.framework，Dart项目代码编译成App.framework，然后嵌入App。

对于Android，Flutter会封装成Flutter.jar。

#### 抓包
flutter网络请求默认没有走系统代理，无法直接抓包，可以设置代理
```
// dio set proxy
(dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (client) {
  client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  client.findProxy = (url) {
    return "PROXY localhost:8888";
  };
};
```



#### 资源文件
Flutter项目创建资源目录，图片分1x、2x、3x，在pubspec.yaml声明依赖；
![-w416](media/15693822571000/15697276391233.jpg)

![-w573](media/15693822571000/15697276583832.jpg)

Flutter是单独导入的资源文件，如果和原生有重复资源，会导致App体积变大，美团实现了资源共用，参考[https://tech.meituan.com/2018/08/09/waimai-flutter-practice.html](https://tech.meituan.com/2018/08/09/waimai-flutter-practice.html)

#### 动态判断Debug、Release
1. assert只在Debug模式下生效，assert((){return true;}());
2. Dart提供了 kReleaseMode 布尔值；
3. 用assert方式判断，Debug部分的代码不会包含在最终的二进制包中，用kReleaseMode判断，Debug部分代码会在二进制包中；

#### 分离develop、production环境

第一种方式：AppConfig，会传给子Widget;
```
class AppConfig extends InheritedWidget {
  AppConfig({
    @required this.apiBaseUrl,
    @required Widget child,
  }) : super(child: child);

  final String apiBaseUrl;// 接口域名

  // 方便其子 Widget 在 Widget 树中找到它
  static AppConfig of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppConfig);
  }
  
  // 判断是否需要子 Widget 更新。由于是应用入口，无需更新
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
```

第二种方式，Flutter运行、打包时指定程序入口，比如main_dev.dart、main.dart。

#### 与原生交互

通过MethodChannel与原生交互。

**Flutter调用原生支付**

Flutter端代码
```
var platform = MethodChannel('com.boqii.flutter.ios.channel.pay');
platform.invokeMapMethod('boqii://pay?orderId=xxxx');
```

iOS端代码
```
FlutterMethodChannel *payChannel = [FlutterMethodChannel methodChannelWithName:@"com.boqii.flutter.ios.channel.pay" binaryMessenger:vc];
[payChannel setMethodCallHandler:^(FlutterMethodCall * _Nonnull call, FlutterResult  _Nonnull result) {
MallOrderPaymentView *paymentView = [[MallOrderPaymentView alloc] initWithFrame:CGRectMake(0, ScreenHeight, ScreenWidth, ScreenHeight)];
        
UIWindow *currentWindow = [[[UIApplication sharedApplication] delegate] window];;
[currentWindow addSubview:paymentView];
        
[UIView animateWithDuration:0.4f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    paymentView.alpha = 1.0f;
    paymentView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
} completion:^(BOOL finished) {
       
}];
```

Flutter会传过来方法名和参数，可以把方法名定义成通用协议 `boqii://pay?orderId=xxx&price=xxx`，原生解析处理；

#### H5页面跳转至原生商品详情页

Flutter端代码：
```
var platform = MethodChannel('com.boqii.flutter.ios.channel.route');
    Future<Null> pushNative(String url) async {
    // invoke method with arguments
    await platform.invokeMapMethod(url);
}
pushNative(request.url);
```
iOS端代码
```
FlutterMethodChannel *routeChannel = [FlutterMethodChannel methodChannelWithName:@"com.boqii.flutter.ios.channel.route" binaryMessenger:vc];
[routeChannel setMethodCallHandler:^(FlutterMethodCall * _Nonnull call, FlutterResult  _Nonnull result) {
    // call.method call.arguments
    NSString *goodsId = [[call.method componentsSeparatedByString:@"="] lastObject];
    MallProductDetailController *vc = [[MallProductDetailController alloc] init];
    vc.goodsId = goodsId;
    [[UIViewController navigationController] pushViewController:vc animated:YES];
}];
```
H5页面加载请求`boqii://route?page=productdetail&goodsID=34005`，Flutter拦截请求，传给原生，原生解析处理；

#### master、stable版本

官方支持的混合开发还未正式发布，是基于Flutter master分支，目前版本是1.10.7-pre19；目前stable分支版本是1.9.1+hotfix3。各版本差异参考 [Release Note](https://github.com/flutter/flutter/releases)。

#### 热更新

Flutter官方已声明不支持热更新，支持热更新理论有两个方式：
1. Dart源码以JIT的形式运行，需要修改Flutter Engine，会严重影响App运行效率；
2. 类似于腾讯的OCS方案，自定义字节码，实现解释器，将Dart代码翻译成字节码，技术难度太大；

业内有通过用Flutter上层抽象DSL，通过解析DSL构建布局，实现了下发DSL动态更改布局。

## 兼容性

系统：Android Jelly Bean, v16, 4.1.x 或更新版本，以及 iOS 8 或更高版本。
设备：iOS 设备（iPhone 4S 或更新版本）和 ARM Android 设备。

## 业内使用情况
闲鱼、美团、京东、腾讯Now、蚂蚁金服、贝壳等企业都在使用，大多是原生、Flutter混合开发。

[2019年6月GMTC全球前端技术大会关于Flutter的分享](https://gmtc.infoq.cn/2019/beijing/schedule)

1. Flutter：最新进展和未来展望；
2. 闲鱼基于Flutter技术的架构演进与创新；
3. Flutter在贝壳的接入实践；
4. Event Loop、Future与Isolate - 单线程模型下的Dart异步编程模式；

## 优缺点

#### 优点：
1. 跨平台(Android、iOS、Web、小程序)；
2. 提高**页面**开发效率；
3. 相对于其他跨平台方案更彻底，性能更好；
4. 社区活跃；

#### 缺点：
1. 学习成本(Dart、Flutter)；
2. **Flutter相当于提供了UI SDK，还是要借助原生的Push、视频、Map等**；
3. 不够稳定，stable版本迭代很快，也不断的推出hotfix版本，混合开发依赖master版本；
4. 官方不支持热更新；
5. Widget与原生控件的差异 & 各种Bug；[5000+ issues](https://github.com/flutter/flutter/issues)
6. 体积增加14M，Flutter SDK + 第三方依赖库 + 重复资源文件，![](media/15693822571000/15694837230136.jpg)

## Todo
1. 渲染时Layout细节；
2. 状态管理 Redux；
3. Router；
4. 插件机制；
5. 图片缓存（每一个Engine维护自己的缓存，Engine之间不共享？）；
6. 原生->Flutter->原生->Flutter 会有Engine复用问题？
7. 埋点体系(页面生命周期不同，埋点方式不同AOP？)；
8. Dart VM;
9. etc；

## Reference
1. [Flutter](https://flutter.dev)
2. [Flutter System Architecture](https://docs.google.com/presentation/d/1cw7A4HbvM_Abv320rVgPVGiUP2msVs7tfGbkgdrTy0I/edit#slide=id.gbb3c3233b_0_162)
3. [Flutter原理与实践](https://tech.meituan.com/2018/08/09/waimai-flutter-practice.html)
4. [Flutter's Rendering Pipeline](https://www.youtube.com/watch?v=UUfXWzp0-DU)
5. [iOS原生、大前端和Flutter分别是怎么渲染的？](https://time.geekbang.org/column/article/101639)
6. [Add Flutter to existing apps](https://github.com/flutter/flutter/wiki/Add-Flutter-to-existing-apps)
7. [Flutter: Don’t Fear the Garbage Collector](https://medium.com/flutter/flutter-dont-fear-the-garbage-collector-d69b3ff1ca30)
8. [并发之痛 Thread，Goroutine，Actor](http://jolestar.com/parallel-programming-model-thread-goroutine-actor/)
9. [聊一聊Flutter线程管理与Dart Isolate机制](https://zhuanlan.zhihu.com/p/40069285)
10. [JIT and AOT](https://juejin.im/entry/5c9455845188252d93207e66)
11. [闲鱼开源 FlutterBoost：实现 Flutter 混合开发](https://www.infoq.cn/article/VBqfCIuwdjtU_CmcKaEu)
12. [闲鱼基于 Flutter 的移动端跨平台应用实践](https://www.infoq.cn/article/xianyu-cross-platform-based-on-flutter)
13. [京东：将 Flutter 扩展到微信小程序端的探索](https://www.infoq.cn/article/ZLauMj3u8bLrIlYvud9F)