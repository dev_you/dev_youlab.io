---
title: 使用 InjectionIII 实现热加载及其原理剖析
date: 2019-04-20
tags: ["iOS", "InjectionIII", "HotReload"]
---

RN 项目改动代码之后，可以通过 Hot Reload 直接刷新页面。在开发调试过程中可以提高效率，iOS 端使用 InjectionIII 也可以达到类似效果。

InjectionIII 是 [John Holdsworth](https://github.com/johnno1962) 开发的工具，一开始是一款 Xcode 插件，后来发展成 Mac App，现在由 Swift 语言编写，支持 OC 和 Swfit 项目。

<!--more-->

项目在以下环境测试正常：

* macOS Mojave 10.14.3
* Xcode 10.2 (10E125)
* iPhone X Simulator 10.2
* InjectionIII 1.5.1
* OC 项目（使用了 ReactCocoa 库）

### 使用指南

1. 在 AppStore 安装 [InjectionIII](https://itunes.apple.com/us/app/injectioniii/id1380446739)
2. 在 AppDelegate 中插入以下代码


    ```
    - (void)setupInjectionIII 
    {
    #if DEBUG
        [[NSBundle bundleWithPath:@"/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle"] load];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTopVC) name:@"INJECTION_BUNDLE_NOTIFICATION" object:nil];
    #endif
    }
- (void)refreshTopVC 
{
        [EMBHudView showHudWithText:@"Hot Reloading..."];
        @try {
            // 获取当前显示的 ViewController，参考：https://stackoverflow.com/a/17578272
            UIViewController *vc = [UIViewController topViewController];
            for (UIView *subView in vc.view.subviews) {
                [subView removeFromSuperview];
            }
            for (CALayer *subLayer in vc.view.layer.sublayers) {
                [subLayer removeFromSuperlayer];
            }
            [vc viewDidLoad];
            [vc viewWillAppear:NO];
            [vc viewDidAppear:NO];
        } @catch (NSException *exception) {
            [EMBHudView showHudWithText:exception.description];
        } @finally {
        
        }
}
    ```
    
3. 运行 InjectionIII，勾选 File Watcher 选项，点击 Open Project 选择项目代码根目录( App 在模拟器启动之后也会提示选择项目目录)

    ![](media/15543601203112/15543619803733.jpg)

4. 使用模拟器运行项目，更改项目代码，按 command + s 保存代码，模拟器就会自动更新当前页面

### 使用限制

1. 只支持模拟器，因为 Hot Reloading 的实现是通过注入动态库实现的，模拟器可以加载任意位置的动态库，真机不可以
2. 对 ReactCocoa 库不友好，原因见下面原理剖析
3. To work, method dispatch must be through the classes "vtable" and not be "direct" i.e. statically linked. This means injection will not work for final methods or methods in final classes or structs.
4. If the file you're injecting as non instance-level variables e.g. singletons they will be reset when you inject the code as the new implementations will refer to the newly loaded version of the class.

### 调试 InjectionIII 

一步步调试是理解 InjectionIII 运行机制的最好方式，可以在 GitHub 下载 [InjectionIII 源代码](https://github.com/johnno1962/InjectionIII)。运行项目可能会遇到的问题：

1. 证书，下图红框内的3个 Target 都需要更改成自己的证书
    ![](media/15543601203112/15543654699115.jpg)

2. New Build System 错误，Xcode -> File -> Project Setting，Build System 更改成 Legacy Build System
    ![](media/15543601203112/15543657307883.jpg)

项目成功运行后，InjectionIII 的图标就会出现在状态栏，但是还差最后一步。前面在 Appdelegate 中插入了下面代码，这是模拟器加载 App Store 版本 InjectionIII 中的 iOSInjectionIII.bundle。

```
[[NSBundle bundleWithPath:@"/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle"] load];
```

我们无法直接调试这个 Bundle 对应的源代码，但是让模拟器加载我们自己编译产生 iOSInjectionIII.bundle，这样就可以在源代码中添加 Log 代码，然后根据 Log 来分析。在 Target InjectionIII 的 Build Phase 找到最下面的 Run Script，添加下面的代码将 Bundle 复制到其他地方

```
cp -rf $SYMROOT/Debug-iphonesimulator/iOSInjection.bundle ~/Downloads/iOSInjection/
```

然后更改 Appdelegate 

```
[[NSBundle bundleWithPath:@"~/Downloads/iOSInjection/iOSInjection.bundle"] load];
```

这样就可以通过 Log 调试 Bundle 对应的源代码
![](media/15543601203112/15543673119548.jpg)


### 原理剖析

1. 模拟器启动，加载 iOSInjection.bundle，Bundle 中包含 iOSInjection 二进制文件；
2. InjectionIII、iOSInjection 通过 Socket 协议通信，分别对应 Server、Client；
3. InjectionIII 监听到项目代码变更时，通知 iOSInjection 哪些文件发生变更；
4. iOSInjection 会查找 DerivedData 目录下最后一次的编译日志，根据日志重新编译发生更改的文件，生成 dylib 动态库并签名；
5. 然后 iOSInjection 载入动态库到运行的 App 里；
6. App 中就有两个同名 Class，通过 Swizzle 把旧 Class 的 class & instance method 替换成新 Class 的；
7. 更新 UI；


几个细节如下：

**Socket 通信**

Server、Client 完成连接后，Client 先发送特定的字符串，Server 做校验。下面代码是 Socket read、wrtite 信息的代码

```
- (int)readInt {
    int32_t anint;
    if (read(clientSocket, &anint, sizeof anint) != sizeof anint)
        return ~0;
    return anint;
}

- (NSString *)readString {
    uint32_t length = [self readInt];
    if (length == ~0)
        return nil;
    char utf8[length + 1];
    if (read(clientSocket, utf8, length) != length)
        return nil;
    utf8[length] = '\000';
    return [NSString stringWithUTF8String:utf8];
}

- (BOOL)writeString:(NSString *)string {
    NSLog(@"write string %@",string);
    const char *utf8 = string.UTF8String;
    uint32_t length = (uint32_t)strlen(utf8);
    if (write(clientSocket, &length, sizeof length) != sizeof length ||
        write(clientSocket, utf8, length) != length)
        return FALSE;
    return TRUE;
}

- (BOOL)writeCommand:(int)command withString:(NSString *)string {
    return write(clientSocket, &command, sizeof command) == sizeof command &&
        (!string || [self writeString:string]);
}
```

**文件变更监听**

文件发生变更后会触发 fileEvents 回调

```
- (instancetype)initWithRoot:(NSString *)projectRoot plugin:(InjectionCallback)callback;
{
    if ((self = [super init])) {
        self.callback = callback;
        static struct FSEventStreamContext context;
        context.info = (__bridge void *)self;
        fileEvents = FSEventStreamCreate(kCFAllocatorDefault,
                                         fileCallback, &context,
                                         (__bridge CFArrayRef) @[ projectRoot ],
                                         kFSEventStreamEventIdSinceNow, .1,
                                         kFSEventStreamCreateFlagUseCFTypes |
                                         kFSEventStreamCreateFlagFileEvents);
        FSEventStreamScheduleWithRunLoop(fileEvents, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
        FSEventStreamStart(fileEvents);
    }

    return self;
}
```

**载入动态库 & 方法替换**

dlopen 载入生成的动态库 tmpfile，返回 dl 指针，dlsym 得到 tmpfile 的动态符号地址

```
@objc func loadAndInject(tmpfile: String, oldClass: AnyClass? = nil) throws -> [AnyClass] {

        print("💉 Loading .dylib ...")
        // load patched .dylib into process with new version of class
        guard let dl = dlopen("\(tmpfile).dylib", RTLD_NOW) else {
            let error = String(cString: dlerror())
            if error.contains("___llvm_profile_runtime") {
                print("💉 Loading .dylib has failed, try turning off collection of test coverage in your scheme")
            }
            throw evalError("dlopen() error: \(error)")
        }
        print("loadAndInject -> %@",dl)
        print("💉 Loaded .dylib - Ignore any duplicate class warning ^")

        if oldClass != nil {
            // find patched version of class using symbol for existing
            var info = Dl_info()
            print("oldClass -> %@",info)
            guard dladdr(unsafeBitCast(oldClass, to: UnsafeRawPointer.self), &info) != 0 else {
                throw evalError("Could not locate class symbol")
            }

            debug(String(cString: info.dli_sname))
            guard let newSymbol = dlsym(dl, info.dli_sname) else {
                throw evalError("Could not locate newly loaded class symbol")
            }

            return [unsafeBitCast(newSymbol, to: AnyClass.self)]
        }
        else {
            // grep out symbols for classes being injected from object file

            try injectGenerics(tmpfile: tmpfile, handle: dl)

            guard shell(command: """
                \(xcodeDev)/Toolchains/XcodeDefault.xctoolchain/usr/bin/nm \(tmpfile).o | grep -E ' S _OBJC_CLASS_\\$_| _(_T0|\\$S|\\$s).*CN$' | awk '{print $3}' >\(tmpfile).classes
                """) else {
                throw evalError("Could not list class symbols")
            }
            guard var symbols = (try? String(contentsOfFile: "\(tmpfile).classes"))?.components(separatedBy: "\n") else {
                throw evalError("Could not load class symbol list")
            }
            symbols.removeLast()

            return Set(symbols.flatMap { dlsym(dl, String($0.dropFirst())) }).map { unsafeBitCast($0, to: AnyClass.self) }
        }
    }
```

```
let newClasses = try SwiftEval.instance.loadAndInject(tmpfile: tmpfile)
let oldClasses = newClasses.map { objc_getClass(class_getName($0)) as! AnyClass }
```

替换旧 Class 的方法

```
static func injection(swizzle newClass: AnyClass?, onto oldClass: AnyClass?) {
        var methodCount: UInt32 = 0
        if let methods = class_copyMethodList(newClass, &methodCount) {
            for i in 0 ..< Int(methodCount) {
                class_replaceMethod(oldClass, method_getName(methods[i]),
                                    method_getImplementation(methods[i]),
                                    method_getTypeEncoding(methods[i]))
            }
            free(methods)
        }
    }
    
// old-school swizzle Objective-C class & instance methods
injection(swizzle: object_getClass(newClass), onto: object_getClass(oldClass))
injection(swizzle: newClass, onto: oldClass)
```

新 Class 已经加载到 App，为什么还要替换旧 Class 呢？每次加载动态库 Log 中都会打印下面信息：
![](media/15543601203112/15543715202193.jpg)

App 存在两个同名 Class，系统调用哪一个是没有定义的，所以需要把旧 Class 的方法替换掉。

**更新 UI**

上面的工作完成之后需要通知模拟器更新 UI，通知方式有两种：

1. 变更的文件中实现了 -(void)injected 方法，会主动调用；
2. 发送通知
 
代码以及相关注释如下，：

```
// injectedClasses 代表需要主动调用 injected 方法的类
var injectedClasses = [AnyClass]()
for cls in oldClasses {
    // 发生变更的 Class 实现了 -(void)injected 方法
    if class_getInstanceMethod(cls, #selector(SwiftInjected.injected)) != nil {
        injectedClasses.append(cls)
        let kvoName = "NSKVONotifying_" + NSStringFromClass(cls)
        if let kvoCls = NSClassFromString(kvoName) {
            injectedClasses.append(kvoCls)
        }
    }
}
// implement -injected() method using sweep of objects in application
if !injectedClasses.isEmpty {
    #if os(iOS) || os(tvOS)
    let app = UIApplication.shared
    #else
    let app = NSApplication.shared
    #endif
    let seeds: [Any] =  [app.delegate as Any] + app.windows
    SwiftSweeper(instanceTask: {
        (instance: AnyObject) in
        if injectedClasses.contains(where: { $0 == object_getClass(instance) }) {
            let proto = unsafeBitCast(instance, to: SwiftInjected.self)
            if SwiftEval.sharedInstance().vaccineEnabled {
                performVaccineInjection(instance)
                proto.injected?()
                return
            }
            // 主动调用 injected 方法
            proto.injected?()

            #if os(iOS) || os(tvOS)
            if let vc = instance as? UIViewController {
                flash(vc: vc)
            }
            #endif
        }
    }).sweepValue(seeds)
}

let notification = Notification.Name("INJECTION_BUNDLE_NOTIFICATION")
NotificationCenter.default.post(name: notification, object: oldClasses)
```

其中 SwiftSweeper 以 Appdelegate 和 UIWindow 为起点，递归它们的实例变量，如果发现其中有发生变更的 Class，并且实现了 -(void)injected，就调用 injected 方法。

这样就可以为 UIViewController 添加分类，实现 injected 方法，在方法内更新 UI，类似于上面提到的 refreshTopVC 方法。

但是我们的项目使用了 ReactCocoa，SwiftSweeper 在递归的过程中碰到 ReactCocoa 的类会闪退，就只能通过监听通知的方式来更新 UI。

另外，代码中的 vaccine 对应 InjectionIII 中的 Enable Vaccine，功能类似于 refreshTopVC 方法。


### 参考

1. [Injection III, the App](http://johnholdsworth.com/injection.html)
2. [Injection, the Missing Manual](https://johnno1962.github.io/InjectionApp/injectionfaq.html)
3. [Injection：iOS热重载背后的黑魔法](https://mp.weixin.qq.com/s/hFnHdOP6pmIwzZck-zXE8g)

