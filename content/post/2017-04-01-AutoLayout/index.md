---
title: AutoLayout
date: 2017-04-01
tags: ["iOS","AutoLayout"]
---

AutoLayout的一些参考资料。

<!--more-->

#### AutoLayout入门

1. [Auto Layout Tutorial in iOS 9 Part 1: Getting Started](http://www.raywenderlich.com/115440/auto-layout-tutorial-in-ios-9-part-1-getting-started-2)
2. [Auto Layout Tutorial in iOS 9 Part 2: Constraints](http://www.raywenderlich.com/115444/auto-layout-tutorial-in-ios-9-part-2-constraints)

#### 第三方库

1. [Masonry](https://github.com/SnapKit/Masonry)
2. [SnapKit (Swift版Masonry)](https://github.com/SnapKit/SnapKit)
3. [Cartography (另外一个Swift版的库)](https://github.com/robb/Cartography)

#### Masonry实践

3. [有趣的Autolayout示例-Masonry实现](http://tutuge.me/2015/05/23/autolayout-example-with-masonry/)
4. [有趣的Autolayout示例2-Masonry实现](http://tutuge.me/2015/08/08/autolayout-example-with-masonry2/)
5. [有趣的Autolayout示例3-Masonry实现](http://tutuge.me/2015/12/14/autolayout-example-with-masonry3/)

#### AutoLayout讲解

1. [深入剖析Auto Layout，分析iOS各版本新增特性](http://www.starming.com/2015/11/03/deeply-analyse-autolayout/)
2. [Content Hugging vs Content Compression Resistance 1](http://stackoverflow.com/questions/15850417/cocoa-autolayout-content-hugging-vs-content-compression-resistance-priority)
3. [Content Hugging vs Content Compression Resistance 2](http://blog.csdn.net/lcg0412/article/details/43114265)
4. [Auto Layout 是怎么进行自动布局的，性能如何](https://time.geekbang.org/column/article/85332)

#### AutoLayout+Cell/ScrollView

1. [iOS 8 自适应 Cell](http://vit0.com/blog/2014/11/13/ios-8-zi-shi-ying-cell/)
2. [优化UITableViewCell高度计算的那些事](http://blog.sunnyxx.com/2015/05/17/cell-height-calculation/)
3. [Technical Note TN2154 UIScrollView And Autolayout](https://developer.apple.com/library/ios/technotes/tn2154/_index.html)
4. [UIScrollView 实践经验](http://tech.glowing.com/cn/practice-in-uiscrollview/)

#### AutoLayout下View动态变化

1. [如何使用Masonry设计复合型cell](http://adad184.com/2015/06/08/complex-cell-with-masonry/)
2. [UIView-FDCollapsibleConstraints](https://github.com/forkingdog/UIView-FDCollapsibleConstraints)

#### UIStackView

1. [iOS 9: Getting Started with UIStackView](http://code.tutsplus.com/tutorials/ios-9-getting-started-with-uistackview--cms-24193)
2. [上面的译文](http://www.cocoachina.com/ios/20150623/12233.html)
3. [FDStackView](https://github.com/forkingdog/FDStackView)
4. [FDStackView —— Downward Compatible UIStackView (Part 1)](http://blog.wtlucky.com/blog/2015/10/09/fdstackview-downward-compatible-uistackview-part-1/)





