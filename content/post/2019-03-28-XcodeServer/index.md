---
title: Xcode Server 持续集成
date: 2019-03-28
tags: ["iOS", "Xcode Server", "CI"]
---

持续集成的工具有 Jenkins、Fastlane 等，但是 Jenkins 界面不友好，配置麻烦，Fastlane 为了适配苹果 API 更新频繁，现在 Xcode 中集成了 Xcode Server，更方便些。下面是配置 Xcode Server 的具体步骤。

<!--more-->

## Xcode Server 配置

Scheme 设置为 Shared
![](media/15480509466244/15480519336717.jpg)

创建 Xcode Server
![屏幕快照 2019-01-21 14.17.59](media/15480509466244/%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7%202019-01-21%2014.17.59.png)

![屏幕快照 2019-01-21 14.19.17](media/15480509466244/%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7%202019-01-21%2014.19.17.png)

创建 Bot
![](media/15480509466244/15480514008538.jpg)

选择刚创建的 Server
![](media/15480509466244/15480520878941.jpg)

配置项目代码访问权限，此项目有两个代码库，iOS代码库和RN代码库。可以选择代码分支。
![](media/15480509466244/15480521686105.jpg)

配置 Bot，Analyze可以不选，节省打包时间。如果需要导出ad-hoc或者AppStore的包，需要额外的plist，手动打包导出的文件夹内就包含ExportOptions.plish，打包上传到AppStore需要把plist文件中的method改成appstore，Configuration设置成Release。

![](media/15480509466244/15480522671556.jpg)

配置打包时间点
![](media/15480509466244/15480529102410.jpg)


自动管理证书
![](media/15480509466244/15480529554565.jpg)

环境变量
![](media/15480509466244/15480529798047.jpg)

## 配置打包前后脚本

添加打包前、打包后执行脚本
![屏幕快照 2019-01-21 14.43.26](media/15480509466244/%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7%202019-01-21%2014.43.26.png)


项目依赖RN项目和Pod，所以需要先把RN项目打包，以及执行Pod install，Pre-Integration Script 如下：

```
# 设置执行环境的语言编码，不设置 pod install 会失败
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# npm pod 目录，不设置会报 npm、pod 命令找不到
export PATH="/usr/local/bin:$PATH"

# 脚本执行目录就是代码根目录
# RN项目在iOS项目同级别，先切换到RN项目打包
cd boqii-rn
git pull
npm install
yarn bundle-ios

cd ../BoqiiMallPro
git pull
pod install
```

打包成 ad-hoc 可以把 IPA 包上传到 fir.im

```
 export PATH="/usr/local/bin:$PATH"
 # IPA 包路径 $XCS_PRODUCT
 fir publish $XCS_PRODUCT
```


打包成 appstore 可以用 altool 上传到 iTunes Connect

```
altool=/Applications/Xcode.app/Contents/Applications/Application\ Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Versions/A/Support/altool
name="xxxxxx"
password="xxxxx"

#验证信息
"$altool" --validate-app -f "$XCS_PRODUCT" -u "$name" -p "$password" --output-format xml

#上传iTunesConnect
"$altool" --upload-app -f "$XCS_PRODUCT" -u "$name" -p "$password" --output-format xml
```
设置完成后可以看打包概览，保存IPA包，编辑Bot
![](media/15480509466244/15480541985897.jpg)


## 邮件通知

Xcode Server 中的邮件设置无效，需要自己写脚本发送邮件，可以使用轻量级的msmtp工具

安装msmtp

```
brew install msmtp
```

在用户根目录创建或修改 .mailrc

```
set sendmail=/usr/local/bin/msmt
```

在用户更目录创建或修改 .msmtprc

```
account boqii
host smtp.boqii.com
tls off
tls_certcheck off
tls_starttls off
port 25
auth login
from user@mail.com
user user@mail.com
password xxxxxxxxxxx

account default: boqii
```

更改 .msmtp 文件的权限

```
chmod 600 .msmtprc
```

测试邮件发送

```
 echo "Hello World" | mail -s "Mail Title" user@email.com      
```

上传至fir之后，将二维码直接添加到邮件内，体验更好，fir publish 添加 -Q 参数，就会在本地生成一张二维码图片

```
fir publish -Q $XCS_PRODUCT
```

Mac下的mail命令无法直接在邮件内添加图片，但是可以添加html格式，把图片内签到html格式即可，有些邮箱不能直接展示html，需要声明下

```
echo "Hello World" | mail -s "$(echo "iOS Team Build Bot\nContent-Type: text/html")" user@email.com
```

由于图片是本地图片，需要将图片base64之后嵌入html

```
# 注意图片路径
baseImage=$(base64 xxx.png)
htmlEmail="<b><div>扫描二维码下载</div></b> <b><div><span><canvas width=\"200\" height=\"200\" style=\"display: none;\"></canvas><img src=\"data:image/png;base64,$baseImage\"style=\"display: block;\"></span></div></b>"
echo $htmlEmail | mail -s "$(echo "iOS Team Build Bot\nContent-Type: text/html")" user@email.com
```

# 参考

1. [fir.im-cli](https://github.com/FIRHQ/fir-cli/blob/master/README.md)
2. [使用 Xcode Server 持续集成 & 打包测试](https://juejin.im/post/5b220a65e51d4558ad724298)
3. [Mac OS X 下用命令行发送邮件](https://my.oschina.net/uhziel/blog/186683)


