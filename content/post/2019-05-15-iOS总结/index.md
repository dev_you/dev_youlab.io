---
title: iOS总结
date: 2019-10-10T18:53:26+08:00
tags: [iOS]
---

做iOS有好几年了，总结下相关知识点。

<!--more-->

### ARC

#### 什么是ARC

MRC：手动引用计数
MRC中有retain、release、autorelease，retain、release分别增加、减少对象的引用计数，autorelease延长对象的生命时间，在当前线程的runloop销毁时释放。函数有返回值时，方法名以alloc、new、copy、mutableCopy开头，调用者负责释放返回的对象，其他的方法自己负责释放对象，这种情况下，为了对象生命周期超过方法调用周期，对象会调用autorelease。

ARC：自动引用计数
不需要自己手动管理，clang做静态分析自动添加retain、release、autorelease并做了一些优化：1. 调用底层C方法的objc_retain、objc_release、objc_autorelease，这个是在编译阶段2. 检查代码如果方法返回的对象调用了autorelease，调用者retain了对象，就不直接调用调用autorelease、retain，而是调用objc_autoreleaseReturnValue和objc_RetainAutoreleaseReturnValue，objc_autoreleaseReturnValue将对象存储在TLS（Thread Local Storage，线程局部存储，将一块内存作为某个线程专有的存储，以key-value的形式进行读写），objc_RetainAutoreleaseReturnValue直接从TLS取值，这样就避免了autorease和retain操作，这个是在运行时做的优化；

ARC 自动清理对象的实例变量，使用C++的析构函数。
ARC 默认不会释放发生异常时try、catch中的对象。

#### 修饰符

__strong 强引用；
__weak 弱引用，变量指向的对象销毁后，变量自动置为nil，
__unsafe_unretained 弱引用，变量指向的对象销毁后，变量不做操作；
__autoreleasing 会把对象注册到autoreleasepool中

__strong __weak __autorelease 保证其指定的变量初始化为nil

__weak修饰的变量指向的对象会自动注册到autoreleasepool，但是每次调用该变量都会注册一次，所以最好再强引用一次

__weak修饰的变量指向的对象销毁时如何将自动变量置为nil？
Runtime维护weak哈希表，Key是对象的地址，Value是弱引用变量的数组，销毁对象时，调用析构函数，查询哈希表，将弱引用变量全部置为nil，然后移除Key。

id的指针或者对象的指针会默认附加上__autoreleasing修饰符 
1. id obj = id __autoreleaseing obj
2. NSStrig **p = NSString * __autoreleasing *p;

解释下面方法的参数为什么定义成 NSError \* \__autoreleasing \* ？

```
- (BOOL)performOperationWithError:(NSError * \__autoreleasing * )error;
```
1. \*\*是为了给error变量赋值，如果可以返回多个值，就不需要这样了；
2. error对象是产生在方法内部的，__autoreleasing是为了延长error对象的生命周期，给外部调用；
3. 对象指针赋值时，修饰符必须一致，编译器用了临时变量做了转化；

#### 桥接

1. __bridge，Foundation -> Core Foundation，ARC仍然有对象的所有权
2. __bridge_unretained，Foundation -> Core Foundation，ARC交出对象的所有权，需要自己释放对象
3. __bridge_transfer，Core Foundation -> Foundation，ARC获取对象的所有权，不需要自己释放对象

#### autoreleasepool 

1. pool和线程一一对应，内部有指针指向当前线程，AutoreleasePoolPage是由双向链表实现的
2. pool在Runloop即将开始时Push，Runloop即将休眠时先Pop再Push，Runloop即将退出时Pop
3. 不是以alloc、new、copy、mutableCopy开始的方法，返回的对象都会被注册到autoreleasepool
4. objc_autoreleasePoolPush创建pool，并插入哨兵对象（nil），并return哨兵对象地址
5. 给对象发送autorelease消息就是将对象的指针入栈
6. AutoreleasePoolPage 4096字节，满了会新建一个Page，next指针指向新Page的栈底
7. objc_autoreleasePoolPop入参是哨兵对象地址，对晚于哨兵加入的对象发送release消息

#### 引用计数的实现：

1. GUNstep的实现是把引用计数保存在对象占用内存块头部的变量中
2. 苹果的实现是HashTable

#### 参考

1. [AutoreleasePool是什么时候销毁的？](https://www.jianshu.com/p/405b8dfc3829)
2. [浅谈 AutoreleasePool 的实现原理](https://juejin.im/post/5b9a32936fb9a05cee1dceb9)
3. [黑幕背后的Autorelease](https://blog.sunnyxx.com/2014/10/15/behind-autorelease/)



### Block

Block是带有自动变量的匿名函数，也是对象。

#### Block捕获变量

Block会捕获Block定义时使用的变量值

1. 局部变量，将使用的局部变量值保存到Block的结构体中；
2. 局部静态变量，将局部静态变量的指针保存到Block的结构体中；
3. 全局静态变量，可以直接访问、修改；
4. 全局变量，可以直接访问、修改

#### Block的对象布局

![](media/15561731762437/15561731900837.jpg)

#### Block的实现

```
int main()
{
	void (^blk)(void) = ^{printf("Blocks\n");};
	blk();
	return 0;
}
```

以上代码会被改写成

```
struct __block_impl (
	void *isa;
	int Flags;
	int Reserved;
	void *FuncPtr;
);

struct __main_block_impl_0 {
	struct __block_impl impl;
	struct __main_block_desc_0 *Desc;

	_main_block_impl_0(void *fp, struct _main_block_desc_0 *desc, int flag=0){
		impl.isa = &_NSConcreteStackBlock;
		impl.Flags = flags;
		impl.FuncPtr = fp;
		Desc = desc;
	}
};

static void _main_block_func_0(struct _main_block_impl_0 * __cself)
{
	printf("Blocks\n");
}

static struct __main_block_desc_0 {
	unsigned long reserved;
	unsigned long Block_size;
} __mian_block_desc_0_DATA = {
	0,
	sizeof(struct __main_block_impl_0)
};

int main()
{
	void (*blk)(void) = (void (*)(void))&__main_block_impl_0((void *)__main_block_func_0, &__main_block_desc_0_DATA);

	(void (*)(struct __block_impl *)) ((struct __block_impl *blk)->FuncPtr)((struct __block_impl *)blk);
}
```

Block结构可以简化成

```
struct _main_block_impl_0 {
	void *isa;
	int Flags;
	int Reserved;
	void *FuncPtr;
	struct __main_block_desc_0 *Desc;
}
```

Block初始化可以简化成

```
isa = &_NSConcreateStackBlock
Flags = 0;
Reserved = 0;
FuncPtr = _main_block_func_0;
Desc = &__main_block_desc_0_DATA;
```


Block调用

```
(*blk->impl.FuncPtr)(blk);
```

#### Block的存储域

1. _NSConcreteStackBlock，栈（除了_NSConcreteGlobalBlock，声明的Block都在栈上），copy时复制到堆
2. _NSConcreteMallocBlock，堆（拷贝栈上的Block到堆上），copy时增加Block的引用计数
3. _NSConcreteGlobalBlock，DATA区（声明全局Block、声明的Block没有使用局部变量），copy时什么也不做

#### ARC下，什么情况Block会被自动拷贝到堆上？

1. 方法返回Block，首先栈上生成Block，然后copy到堆上，并用objc_autoReleaseReturnValue返回堆上的Block，栈上的Block会销毁；
2. Cocoa Touch中方法名包含usingBlock以及GCD的API；
3. 将Block赋值给类的__strong修饰符的id类型的类或Block类型成员变量时；
4. 调用Block的copy方法；

#### _block的实现

```
__block int val = 10;
```
会转化成以下代码：

```
struct __Block_byref_val_0 {
	void *__isa;
	__Block_byref_val_0 * __forwarding;
	int __flags;
	int __size;
	int val;
};

struct __main_block_impl_0 {
	struct __block_impl impl;
	struct __main_block_desc_0 *Desc;
	__Block_byref_val_0 *val;

	_main_block_impl_0(void *fp, struct _main_block_desc_0 *desc, __Block_byref_val_0 *_val, int flag=0) : val(_val->__forwarding) {
		impl.isa = &_NSConcreateStackBlock;
		impl.Flags = flags;
		impl.FuncPtr = fp;
		Desc = desc;
	}
};

static struct __main_block_desc_0 {
	unsigned long reserved;
	unsigned long Block_size;
	void (^copy)(struct __main_block_impl_0 *, struct __main_block_impl_0 *);
	void (^dispose)(struct __main_block_impl_0 *);
} __mian_block_desc_0_DATA = {
	0,
	sizeof(struct __main_block_impl_0),
	__main_block_copy_0,
	__main_block_dispose_0,
};

static void __main_block_copy_0(struct __main_block_impl_0 *dst, struct __main_block_impl_0 *src) {
	__Block_object_assign(&dst->val, src->val, BLOCK_FIELD_IS_BYREF);
}

static void __main_block_dispose_0(struct __main_block_impl_0 *src) {
	__Block_object_dispose(src->val, BLOCK_FIELD_IS_BYREF);
}
```

1. Block的_main_block_impl_0中保存__block的指针，和局部静态变量有点类似；
2. __main_block_desc_0中多了copy、dispose

#### __forwarding存的的原因？

Block从栈上拷贝到堆，__block变量也会拷贝到堆上，栈上的__block变量的__forwarding指向堆上的__block变量，堆上的__block变量的__forwarding指向自身，那么无论访问栈还是堆上__block变量都是正确的


#### Block的copy、dispose

1. Block持有捕获的对象和__block变量；
2. Block从栈复制到堆上时调用copy将捕获的对象和__block变量也拷贝到堆；
3. 堆上的Block被废弃时，调用dispose释放捕获的对象和__block变量；
4. copy、dispose方法需要区分__block和对象，分别对应的类型是BLOCK_FIELD_IS_BYREF、BLOCK_FIELD_IS_OBJECT


### GCD

#### dispatch_async/sync

1. sync等待task执行完返回，会阻塞当前线程，async将task加入queue之后直接返回；
2. 主线程调用dispatch_sync会造成死锁；

#### dispatch_queue_crete

```
// 第二个参数 DISPATCH_QUEUE_SERIAL(NULL)、DISPATCH_QUEUE_CONCURRENT
// 创建一个串行队列，就会创建一个线程
// GCD API方法名包含crete的，需要释放Queue(iOS 6之后由ARC管理)
// Block通过dispatch_retain持有Queue
dispatch_queue_t queue = dispatch_queue_crete("com.example.queue", DISPATCH_QUEUE_SERIAL);
dispatch_async(queue, ^{NSLog(@"Serial Queue");});
dispatch_release(queue);    // 编译错误
```

#### Main/Global Dispatch Queue

1. Main Dispatch Queue是串行队列；(主线程执行)
2. Global Dispatch Queue是并行队列；
3. Global 优先级：High、Default、Low、Background；
4. Global QOS优先级：USER_INTERACTIVE、USER_INITIATED、DEFAULT、UTILITY、BACKGROUND
4. Main、Global执行dispatch_retain、dispatch_release没有效果；

优先级对应关系：
DISPATCH_QUEUE_PRIORITY_HIGH:         QOS_CLASS_USER_INITIATED
DISPATCH_QUEUE_PRIORITY_DEFAULT:      QOS_CLASS_DEFAULT
DISPATCH_QUEUE_PRIORITY_LOW:          QOS_CLASS_UTILITY
DISPATCH_QUEUE_PRIORITY_BACKGROUND:   QOS_CLASS_BACKGROUND


#### dispatch_set_target_queue

dispatch_set_target_queue(queue1, queue2);
1. dispatch_queue_crete创建的queue和Global Queue Default优先级一样；
2. 设置queue1的优先级和queue2的优先级一样；
3. queue1为Main、Global时行为未知；
4. queue2为串行队列，多个串/并行队列的target是queue2，这些队列串行执行；

#### dispatch_after

在N秒后将task加入到指定的Queue中，所以可能有延迟。

#### dispatch_group

```
dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
dispatch_group_t group = dispatch_group_create();
dispatch_group_async(group, queue, ^{NSLog(@"block0");});
dispatch_group_async(group, queue, ^{NSLog(@"block1");});
dispatch_group_async(group, queue, ^{NSLog(@"block2");});
dispatch_group_notify(group, dispatch_get_main_queue(), ^{NSLog(@"done");});
long result = dispatch_group_wait(group, time);
// log: block1, block0, block2, done
```

1. queue是并行队列，block0、block1、block2顺序不确定；
2. group可以对应多个queue；
3. group中的task全部执行完后，notify会将task加入到指定的queue中；
4. wait会阻塞当前线程，第二个参数指超时时间，可以使用DISPATCH_TIME_FOREVER永久等待；
5. wait未超时执行完任务result为0；

#### dispatch_barrier_async/sync

```
// 读取并发，写入单独执行
_queue = dispatch_queue_create("com.example.gcd", DISPATCH_QUEUE_CONCURRENT);

- (void)someString {
	__block NSString *localSomeString;
	dispatch_sync(_queue, ^{
		localSomeString = _someString;
	});
	return localSomeString;
}

- (void)setSomeString:(NSString *)someString {
	dispatch_barrier_async(_queue, ^{
		_someString = someString;
	});
}
```

1. 使用barrier API需要新建一个并行队列，Global队列无效，串行队列没意义；
2. barrier中的保证的是执行的顺序，async会立即返回，执行下面的方法，如下

```
dispatch_async(_queue, ^{NSLog("1")});
dispatch_async(_queue, ^{NSLog("2")});
dispatch_async(_queue, ^{NSLog("3")});
dispatch_barrier_async(_queue,^NSLog("3")});
NSLog("abc");
dispatch_async(_queue, ^{NSLog("4")});
dispatch_async(_queue, ^{NSLog("5")});
dispatch_async(_queue, ^{NSLog("6")});
// result: 123乱序，3和abc的顺序无法保证，然后456乱序
// barrier_async立即返回，把下面的task加入队列中，但执行是在barrier_async执行完成之后执行
```

#### dispatch_apply

按指定的次数将task加入指定的queue，并等待处理结束；

#### dispatch_suspend/dispatch_resume

暂停/回复指定的queue

#### dispatch semaphore

```
dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
// 初始信号量为1，第一次不等待；
dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
NSMutableArray *array = [NSMutableArray array];
for(int i=0; i<10; i++)
{
    dispatch_async(queue, ^{
        // 信号量为0时等待，大于0时减1
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        [array addObject:@(i)];
        // 信号量加1
        dispatch_semaphore_signal(semaphore);
    });
}
```

#### dispatch_once

单例模式常用，线程安全

#### dispatch I/O

读取较大文件时，将文件分割成合适的大小并使用Global Dispatch Queue并列读取，加快文件读取速度；

#### GCD实现

1. GCD API的实现在libdispatch中，均是C函数；
2. dispatch queue是通过结构体和链表，被实现为FIFO，管理加入的Block；
3. Block先加入dispatch_continuation_t中，然后再加入FIFO；
4. dispatch_continuation_t保存Block所属Group等信息；

#### dispatch source

![](media/15566953407175/15566961368274.jpg)


#### dispatch_queue_set_specfic：
QueueA、QueueB是串行序列，QueueB的Target是QueueA，下面代码会发生死锁

```
- (void)deaklock {
	dispatch_sync(QueueB, ^{
		doSomething();
		// 死锁，解决办法是QueueA设置特有数据，然后在这里根据Key看是否能获取到特有数据
		// 如果能获取到，不能在QueueA执行
		dispatch_sync(QueueA, ^{
			doAnotherThing();
		});
	});
}
```
dispatch_queue_set_specfic可以给queueA设置特有数据(有点像associated object)，在其他队列中根据是否可以获取特有数据来判断是不是在queueA上


#### NSOperationQueue vs GCD

1. OperationQueue基于GCD实现，GCD的性能消耗低，OperationQueue更灵活；
2. GCD不能取消task，OperationQueue可以取消Operation，实现方式是Operation有isCancelled状态，执行时可以根据判断该状态是否继续执行；
3. Operation有四种状态：Ready、Executing、Cancelled、Finished，可以监听其状态；
4. Operation之间可以添加依赖，GCD通过串行队列、Barrier、Group也可以实现；
5. Operation可以设置优先级，GCD中Queue才能设置优先级；
6. OperationQueue创建非主队列是并行的，通过maxConcurrentOperationCount可以实现串行；
7. NSOperation添加多个Block是并发执行的；



#### 并发编程中容易出现的问题：

1. 竞态条件（Race Condition）。指两个或两个以上线程对共享的数据进行读写操作时，最终的数据结果不确定的情况。
2. 优先倒置（Priority Inverstion）。指低优先级的任务会因为各种原因先于高优先级任务执行。
3. 死锁问题（Dead Lock）。指两个或两个以上的线程，它们之间互相等待彼此停止执行，以获得某种资源，但是没有一方会提前退出的情况。


#### 死锁

```
// 在主线程中同步操作（主线程是串行并列）
dispatch_sync(dispatch_get_main_queue(), ^{
    
});

// 串行队列中同步操作
dispatch_sync(serialQueue, ^{
    dispatch_sync(serialQueue, ^{
    
    });
});

// 串行队列中异步、同步操作
dispatch_async(serialQueue, ^{
    dispatch_sync(serialQueue, ^{
    
    });
});

// 并行队列的target是串行队列，并行队列中将同步任务添加到串行队列
dispatch_set_target(concurrentQueue, serailQueue);
dispatch_async(concurrentQueue, ^{
    dispatch_sync(serialQueue, ^{
        
    });
});
```

#### 各种锁

##### 自旋锁

自旋锁的实现理论上来说只要定义一个全局变量，用来表示锁的可用情况即可，确保lock的检测和赋值是源自操作，伪代码如下:

```
bool lock = false; // 一开始没有锁上，任何线程都可以申请锁  
do {  
    while(test_and_set(&lock); // test_and_set 是一个原子操作
        Critical section  // 临界区
    lock = false; // 相当于释放锁，这样别的线程可以进入临界区
        Reminder section // 不需要锁保护的代码        
}
```

##### 信号量

dispatch_semaphore_t 的实现最终会调用到 sem_wait 方法，这个方法在 glibc 中被实现如下:

```
int sem_wait (sem_t *sem) {  
  int *futex = (int *) sem;
  if (atomic_decrement_if_positive (futex) > 0)
    return 0;
  int err = lll_futex_wait (futex, 0);
    return -1;
)
```

首先会把信号量的值减一，并判断是否大于零。如果大于零，说明不用等待，所以立刻返回。具体的等待操作在 lll_futex_wait 函数中实现，lll 是 low level lock 的简称。这个函数通过汇编代码实现，调用到 SYS_futex 这个系统调用，使线程进入睡眠状态，主动让出时间片。

##### 互斥锁

pthread_mutex 表示互斥锁。互斥锁的实现原理与信号量非常相似，不是使用忙等，而是阻塞线程并睡眠，需要进行上下文切换。互斥锁的常见用法如下:

```
pthread_mutexattr_t attr;  
pthread_mutexattr_init(&attr);  
pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);  // 定义锁的属性

pthread_mutex_t mutex;  
pthread_mutex_init(&mutex, &attr) // 创建锁

pthread_mutex_lock(&mutex); // 申请锁  
    // 临界区
pthread_mutex_unlock(&mutex); // 释放锁  
```

锁的类型可以有 PTHREAD_MUTEX_NORMAL、PTHREAD_MUTEX_ERRORCHECK、PTHREAD_MUTEX_RECURSIVE。

一般情况下，一个线程只能申请一次锁，也只能在获得锁的情况下才能释放锁，多次申请锁或释放未获得的锁都会导致崩溃。假设在已经获得锁的情况下再次申请锁，线程会因为等待锁的释放而进入睡眠状态，因此就不可能再释放锁，从而导致死锁。

然而这种情况经常会发生，比如某个函数申请了锁，在临界区内又递归调用了自己。辛运的是 pthread_mutex 支持递归锁，也就是允许一个线程递归的申请锁，只要把 attr 的类型改成 PTHREAD_MUTEX_RECURSIVE 即可。

##### NSLock

NSLock 是 Objective-C 以对象的形式暴露给开发者的一种锁，它的实现非常简单，通过宏，定义了 lock 方法:

```
#define    MLOCK \
- (void) lock\
{\
  int err = pthread_mutex_lock(&_mutex);\
  // 错误处理 ……
}
```

NSLock 只是在内部封装了一个 pthread_mutex，属性为 PTHREAD_MUTEX_ERRORCHECK，它会损失一定性能换来错误提示。

这里使用宏定义的原因是，OC 内部还有其他几种锁，他们的 lock 方法都是一模一样，仅仅是内部 pthread_mutex 互斥锁的类型不同。通过宏定义，可以简化方法的定义。

NSLock 比 pthread_mutex 略慢的原因在于它需要经过方法调用，同时由于缓存的存在，多次方法调用不会对性能产生太大的影响。

##### NSCondition

NSCondition 的底层是通过条件变量(condition variable) pthread_cond_t 来实现的。条件变量有点像信号量，提供了线程阻塞与信号机制，因此可以用来阻塞某个线程，并等待某个数据就绪，随后唤醒线程，比如常见的生产者-消费者模式。

```
void consumer () { // 消费者  
    pthread_mutex_lock(&mutex);
    while (data == NULL) {
        pthread_cond_wait(&condition_variable_signal, &mutex); // 等待数据
    }
    // --- 有新的数据，以下代码负责处理 ↓↓↓↓↓↓
    // temp = data;
    // --- 有新的数据，以上代码负责处理 ↑↑↑↑↑↑
    pthread_mutex_unlock(&mutex);
}

void producer () {  
    pthread_mutex_lock(&mutex);
    // 生产数据
    pthread_cond_signal(&condition_variable_signal); // 发出信号给消费者，告诉他们有了新的数据
    pthread_mutex_unlock(&mutex);
}
```

在以上给出的生产者-消费者模式的代码中， pthread_cond_wait 方法的本质是锁的转移，消费者放弃锁，然后生产者获得锁，同理，pthread_cond_signal 则是一个锁从生产者到消费者转移的过程。

NSCondition 其实是封装了一个互斥锁和条件变量， 它把前者的 lock 方法和后者的 wait/signal 统一在 NSCondition 对象中，暴露给使用者:

```
- (void) signal {
  pthread_cond_signal(&_condition);
}

// 其实这个函数是通过宏来定义的，展开后就是这样
- (void) lock {
  int err = pthread_mutex_lock(&_mutex);
}
```

##### NSRecursiveLock

递归锁也是通过 pthread_mutex_lock 函数来实现，在函数内部会判断锁的类型，如果显示是递归锁，就允许递归调用，仅仅将一个计数器加一，锁的释放过程也是同理。

NSRecursiveLock 与 NSLock 的区别在于内部封装的 pthread_mutex_t 对象的类型不同，前者的类型为 PTHREAD_MUTEX_RECURSIVE。

##### NSConditionLock

NSConditionLock 借助 NSCondition 来实现，它的本质就是一个生产者-消费者模型。“条件被满足”可以理解为生产者提供了新的内容。NSConditionLock 的内部持有一个 NSCondition 对象，以及 _condition_value 属性，在初始化时就会对这个属性进行赋值:

```
// 简化版代码
- (id) initWithCondition: (NSInteger)value {
    if (nil != (self = [super init])) {
        _condition = [NSCondition new]
        _condition_value = value;
    }
    return self;
}
```

它的 lockWhenCondition 方法其实就是消费者方法:

```
- (void) lockWhenCondition: (NSInteger)value {
    [_condition lock];
    while (value != _condition_value) {
        [_condition wait];
    }
}
```

对应的 unlockWhenCondition 方法则是生产者，使用了 broadcast 方法通知了所有的消费者:

```
- (void) unlockWithCondition: (NSInteger)value {
    _condition_value = value;
    [_condition broadcast];
    [_condition unlock];
}
```

##### @synchronized

这其实是一个OC层面的锁，主要是通过牺牲性能换来语法上的简洁与可读。我们知道 @synchronized 后面需要紧跟一个 OC 对象，它实际上是把这个对象当做锁来使用。

##### 参考

1. 摘抄自：[深入理解 iOS 开发中的锁](https://bestswifter.com/ios-lock/)

### AutoLayout 原理

![](media/15569388145197/15569391032787.jpg)


#### Auto Layout 的生命周期

![](media/15569388145197/15569391249760.jpg)
![](media/15569388145197/15569391616425.png)
![](media/15569388145197/15569394395531.jpg)

#### 尽量少使用systemLayoutSizeFitting

使用systemLayoutSizeFitting得到Size，会创建、销毁Layout Engine，有性能问题；
![](media/15569388145197/15569395586925.jpg)


### Runtime

#### 基础数据结构

```
// objc_msgSend
id objc_msgSend(id self, SEL op, ...)

// SEL 映射到方法的C字符串
typedef struct objc_selector *SEL;

struct objc_class {
    struct objc_class *isa;
};
struct objc_object {
    struct objc_class *isa;
};
 
typedef struct objc_class *Class; //类  (class object)
typedef struct objc_object *id;   //对象 (instance of class)

//Class的实际结构
struct _class_t {
	struct _class_t *isa;        //isa指针
	struct _class_t *superclass; //父类
	void *cache;                 //方法缓存
	void *vtable;
	struct _class_ro_t *ro;     //Class包含的信息
};

// Class包含的信息
struct _class_ro_t {
	unsigned int flags;
	unsigned int instanceStart;
	unsigned int instanceSize;
	unsigned int reserved;
	const unsigned char *ivarLayout;
	const char *name;                                 //类名
	const struct _method_list_t *baseMethods;         //方法列表
	const struct _objc_protocol_list *baseProtocols;  //协议列表
	const struct _ivar_list_t *ivars;                 //ivar列表
	const unsigned char *weakIvarLayout;
	const struct _prop_list_t *properties;            //属性列表
};

```

#### 对象模型

![](media/15568592542859/15568676286774.jpg)

1. object是class的实例，class是metaclass的实例；
2. 实例方法保存在class内，类方法保存在metaclass内；

#### 消息发送

1. 检测selector是否要忽略(如果支持GC，忽略retain等方法)；
2. 判断receiver是否为nil;
3. 从Cache中查找IMP；
4. 查找类的方法列表，父类的方法列表，直到NSObject，找到缓存；
5. 消息转发，方法不缓存；

#### 消息转发

![](media/15568592542859/15568688616802.png)

#### 方法缓存

```
struct objc_cache {
    uintptr_t mask;     // 最多缓存多少
    uintptr_t occupied; // hash表被占用了多少
    cache_entry *buckets[1];    // 用数组表示的hash表
};
typedef struct {
    SEL name;     // 被缓存的方法名字
    void *unused;
    IMP imp;  // 方法实现
} cache_entry;
```

1. 方法缓存是用hash表实现的；
2. 方法缓存都存在class上；
3. 方法缓存没有大小限制，hash表会增长；

#### Associated Objects

```
// SELs are guaranteed to be unique and constant, you can use _cmd as the key 
void objc_setAssociatedObject ( id object, const void *key, id value, objc_AssociationPolicy policy );
id objc_getAssociatedObject ( id object, const void *key );
void objc_removeAssociatedObjects ( id object );

enum {
   OBJC_ASSOCIATION_ASSIGN  = 0,
   OBJC_ASSOCIATION_RETAIN_NONATOMIC  = 1,
   OBJC_ASSOCIATION_COPY_NONATOMIC  = 3,
   OBJC_ASSOCIATION_RETAIN  = 01401,
   OBJC_ASSOCIATION_COPY  = 01403
};
```

Associated Objects的实现：

1. 关联对象其实就是 ObjcAssociation 对象；
2. 关联对象由 AssociationsManager 管理并在  AssociationsHashMap 存储；
3. 对象的指针以及其对应 ObjectAssociationMap 以键值对的形式存储在 AssociationsHashMap 中；
4. ObjectAssociationMap 则是用于存储关联对象的数据结构；
5. 每一个对象都有一个标记位 has_assoc 指示对象是否含有关联对象；
6. Associated Object在对象的析构函数中释放；

#### Method Swizzling

```
#import <objc/runtime.h> 
 
@implementation UIViewController (Tracking) 
 
+ (void)load { 
    static dispatch_once_t onceToken; 
    dispatch_once(&onceToken, ^{ 
        Class aClass = [self class]; 
 		          
        SEL originalSelector = @selector(viewWillAppear:); 
        SEL swizzledSelector = @selector(xxx_viewWillAppear:); 
 
        Method originalMethod = class_getInstanceMethod(aClass, originalSelector); 
        Method swizzledMethod = class_getInstanceMethod(aClass, swizzledSelector); 
 
        BOOL didAddMethod = 
            class_addMethod(aClass, 
                originalSelector, 
                method_getImplementation(swizzledMethod), 
                method_getTypeEncoding(swizzledMethod)); 
 
        if (didAddMethod) { 
            class_replaceMethod(aClass, 
                swizzledSelector, 
                method_getImplementation(originalMethod), 
                method_getTypeEncoding(originalMethod)); 
        } else { 
            method_exchangeImplementations(originalMethod, swizzledMethod); 
        } 
    }); 
} 
 
#pragma mark - Method Swizzling 
 
- (void)xxx_viewWillAppear:(BOOL)animated { 
    [self xxx_viewWillAppear:animated]; 
    NSLog(@"viewWillAppear: %@", self); 
} 
 
@end
```

#### [self class]、objc_getClass(self)、objc_getClass([self class)]

1. 当 self 为实例对象时，[self class] 与 object_getClass(self) 等价，因为前者会调用后者。object_getClass([self class]) 得到元类。
2. 当 self 为类对象时，[self class] 返回值为自身，还是 self。object_getClass(self) 与 object_getClass([self class]) 等价。

#### category vs extension

1. extension在编译器决定，category在runtime决定；
2. 必须有class的源码才能添加extension；
3. extension可以添加实例变量，加载category时，对象的内存布局已经确定，所以不能添加实例变量；
4. category通过关联对象实现实例变量的效果

#### category 加载

category结构

```
typedef struct category_t {
    const char *name;   // 类名
    classref_t cls;     // category名
    struct method_list_t *instanceMethods;  // 添加的实例方法
    struct method_list_t *classMethods;     // 添加的类方法
    struct protocol_list_t *protocols;      // 实现的协议
    struct property_list_t *instanceProperties; // 添加的property
} category_t;
```

1. 编译器生成 struct category_t \_OBJC\_\$_CATEGORY_ClassName_\$_CategoryName，Category不能重名；
2. 编译器生成了一个数组，存放category，存在__DATA段下的__objc_catlistsection里；
3. objc runtime的加载入口是一个叫_objc_init的方法，在library加载前由libSystem dyld调用，进行初始化操作；
4. 调用map_images方法将文件中的imagemap到内存；
5. 调用_read_images方法初始化map后的image，这里面干了很多的事情，像load所有的类、协议和category；
6. 把category的实例方法、协议以及属性添加到类上，把category的类方法和协议添加到类的metaclass；
7. category方法是添加进类的方法之前的，也就是说，如果原来类的方法是a,b,c，category的方法是1,2,3，那么插入之后的方法将会是1,2,3,a,b,c，也就是说，原来类的方法被category的方法覆盖了，但被覆盖的方法确实还在那里。
8. category的load方法晚于class的load方法；

#### load vs initialize 

1. 加载时间不同，load是App启动加载类的时候调用，initialize是懒加载，第一次向class发送消息时调用；
2. 调用时间的不同导致能使用的资源不同，load调用时不能使用静态变量；
3. 不会调用父类的load方法，会调用category的load方法；
4. 调用父类的initialize方法，复写此方法要检查是不是当前类

#### OC中怎么添加协议的默认实现？
Swift中可以通过extension给协议添加默认实现，OC中可以通过Runtime和\__attribute\__((constructor))给协议添加默认实现。

\__attribute\__((constructor))修饰的函数，会在Runtime加载完所有类之后，main函数执行之前运行。创建一个协议对应的临时类，实现协议的方法。在\__attribute\__((constructor))修饰的函数中遍历所有类遵循的协议，如果有对应的临时类，就把临时类的方法添加到类中；

[ProtocolKit](https://github.com/forkingdog/ProtocolKit)实现了此功能。

#### App启动流程

1. 加载App可执行文件
2. 加载dyld
3. dyld递归加载可执行文件依赖的动态库(otool -L)
    1. load image 一个ImageLoader加载一个可执行文件(.o文件)
    2. rebase image 调整动态库内部指针，性能瓶颈在I/O操作
    3. bind image 调整动态库外部指针，性能瓶颈在CPU
    4. objc setup class registration、category registration、selctor uniquing
    5. initializers load函数、C++构造函数、非基本类型全局变量的创建
4. main()
5. didFinishLaunching()

启动优化案例：

1. [今日头条iOS客户端启动速度优化](https://techblog.toutiao.com/2017/01/17/iosspeed/#more)
2. [优化 App 的启动时间](http://yulingtianxia.com/blog/2016/10/30/Optimizing-App-Startup-Time/)
3. [iOS App 启动性能优化](https://mp.weixin.qq.com/s?__biz=MzA3NTYzODYzMg==&mid=2653579242&idx=1&sn=8f2313711f96a62e7a80d63851653639&chksm=84b3b5edb3c43cfb08e30f2affb1895e8bac8c5c433a50e74e18cda794dcc5bd53287ba69025&mpshare=1&scene=1&srcid=081075Vt9sYPaGgAWwb7xd1x&key=4b95006583a3cb388791057645bf19a825b73affa9d3c1303dbc0040c75548ef548be21acce6a577731a08112119a29dfa75505399bba67497ad729187c6a98469674924c7b447788c7370f6c2003fb4&ascene=0&uin=NDA2NTgwNjc1&devicetype=iMac16%2C2+OSX+OSX+10.12.6+build(16G29)&version=12020110&nettype=WIFI&fontScale=100&pass_ticket=IDZVtt6EyfPD9ZLcACRVJZYH8WaaMPtT%2BF3nfv7yZUQBCMKM4H1rDCbevGd7bXoG)
4. [iOS启动优化](http://lingyuncxb.com/2018/01/30/iOS启动优化/)
5. [支付宝启动优化](https://yq.aliyun.com/articles/672326)

####  main 函数之前发生了什么 

[iOS 程序 main 函数之前发生了什么](https://blog.sunnyxx.com/2014/08/30/objc-pre-main/)


#### self vs super

下面的代码输出什么？

```
@implementation Son : Father
- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"%@", NSStringFromClass([self class]));
        NSLog(@"%@", NSStringFromClass([super class]));
    }
    return self;
}
@end
// result 
NSStringFromClass([self class]) = Son
NSStringFromClass([super class]) = Son
```
objc中super是编译器标示符，并不像self一样是一个对象，遇到向super发的方法时会转译成objc_msgSendSuper(...)，而参数中的对象还是self，于是从父类开始沿继承链寻找- class这个方法，最后在NSObject中找到（若无override），此时，[self class]和[super class]已经等价了。


#### 参考：
1. [Objective-C Class Loading and Initialization](https://www.mikeash.com/pyblog/friday-qa-2009-05-22-objective-c-class-loading-and-initialization.html)
2. [Objective-C Runtime](http://yulingtianxia.com/blog/2014/11/05/objective-c-runtime/#Method)


### Runloop

RunLoop是让线程能随时处理事件但并不退出的一种机制，管理需要处理的事件和消息。线程启动Runloop，就会一直处于“接受消息->等待->处理”的循环中，知道Runloop接受到退出消息。

OSX/iOS 系统中，提供了两个这样的对象：NSRunLoop 和 CFRunLoopRef。CFRunLoopRef 是在 CoreFoundation 框架内的，它提供了纯 C 函数的 API，所有这些 API 都是线程安全的。NSRunLoop 是基于 CFRunLoopRef 的封装，提供了面向对象的 API，但是这些 API 不是线程安全的。

#### RunLoop 与线程的关系

线程和 RunLoop 之间是一一对应的，其关系是保存在一个全局的 Dictionary 里。线程刚创建时并没有 RunLoop，如果你不主动获取，那它一直都不会有。RunLoop 的创建是发生在第一次获取时，RunLoop 的销毁是发生在线程结束时。你只能在一个线程的内部获取其 RunLoop（主线程除外）。

主线程的Runloop是默认启动的，其他线程的Runloop默认是没有启动的。

伪代码如下:

```
/// 全局的Dictionary，key 是 pthread_t， value 是 CFRunLoopRef
static CFMutableDictionaryRef loopsDic;
/// 访问 loopsDic 时的锁
static CFSpinLock_t loopsLock;
 
/// 获取一个 pthread 对应的 RunLoop。
CFRunLoopRef _CFRunLoopGet(pthread_t thread) {
    OSSpinLockLock(&loopsLock);
    
    if (!loopsDic) {
        // 第一次进入时，初始化全局Dic，并先为主线程创建一个 RunLoop。
        loopsDic = CFDictionaryCreateMutable();
        CFRunLoopRef mainLoop = _CFRunLoopCreate();
        CFDictionarySetValue(loopsDic, pthread_main_thread_np(), mainLoop);
    }
    
    /// 直接从 Dictionary 里获取。
    CFRunLoopRef loop = CFDictionaryGetValue(loopsDic, thread));
    
    if (!loop) {
        /// 取不到时，创建一个
        loop = _CFRunLoopCreate();
        CFDictionarySetValue(loopsDic, thread, loop);
        /// 注册一个回调，当线程销毁时，顺便也销毁其对应的 RunLoop。
        _CFSetTSD(..., thread, loop, __CFFinalizeRunLoop);
    }
    
    OSSpinLockUnLock(&loopsLock);
    return loop;
}
 
CFRunLoopRef CFRunLoopGetMain() {
    return _CFRunLoopGet(pthread_main_thread_np());
}
 
CFRunLoopRef CFRunLoopGetCurrent() {
    return _CFRunLoopGet(pthread_self());
}
```

#### Runloop Mode

在 CoreFoundation 里面关于 RunLoop 有5个类:

CFRunLoopRef
CFRunLoopModeRef
CFRunLoopSourceRef
CFRunLoopTimerRef
CFRunLoopObserverRef

其中 CFRunLoopModeRef 类并没有对外暴露，只是通过 CFRunLoopRef 的接口进行了封装。他们的关系如下:

![](media/15569334563848/15569362385410.png)

一个 RunLoop 包含若干个 Mode，每个 Mode 又包含若干个 Source/Timer/Observer。每次调用 RunLoop 的主函数时，只能指定其中一个 Mode，这个Mode被称作 CurrentMode。如果需要切换 Mode，只能退出 Loop，再重新指定一个 Mode 进入。这样做主要是为了分隔开不同组的 Source/Timer/Observer，让其互不影响。

CFRunLoopSourceRef 是事件产生的地方。Source有两个版本：Source0 和 Source1。

1. Source0 只包含了一个回调（函数指针），它并不能主动触发事件。使用时，你需要先调用 CFRunLoopSourceSignal(source)，将这个 Source 标记为待处理，然后手动调用 CFRunLoopWakeUp(runloop) 来唤醒 RunLoop，让其处理这个事件。
2. Source1 包含了一个 mach_port 和一个回调（函数指针），被用于通过内核和其他线程相互发送消息。这种 Source 能主动唤醒 RunLoop 的线程，其原理在下面会讲到。

CFRunLoopTimerRef 是基于时间的触发器，它和 NSTimer 是toll-free bridged 的，可以混用。其包含一个时间长度和一个回调（函数指针）。当其加入到 RunLoop 时，RunLoop会注册对应的时间点，当时间点到时，RunLoop会被唤醒以执行那个回调。

CFRunLoopObserverRef 是观察者，每个 Observer 都包含了一个回调（函数指针），当 RunLoop 的状态发生变化时，观察者就能通过回调接受到这个变化。可以观测的时间点有以下几个：

```
typedef CF_OPTIONS(CFOptionFlags, CFRunLoopActivity) {
    kCFRunLoopEntry         = (1UL << 0), // 即将进入Loop
    kCFRunLoopBeforeTimers  = (1UL << 1), // 即将处理 Timer
    kCFRunLoopBeforeSources = (1UL << 2), // 即将处理 Source
    kCFRunLoopBeforeWaiting = (1UL << 5), // 即将进入休眠
    kCFRunLoopAfterWaiting  = (1UL << 6), // 刚从休眠中唤醒
    kCFRunLoopExit          = (1UL << 7), // 即将退出Loop
};
```

上面的 Source/Timer/Observer 被统称为 mode item，一个 item 可以被同时加入多个 mode。但一个 item 被重复加入同一个 mode 时是不会有效果的。如果一个 mode 中一个 item 都没有，则 RunLoop 会直接退出，不进入循环。

CFRunLoopMode 和 CFRunLoop 的结构大致如下：

```
struct __CFRunLoopMode {
    CFStringRef _name;            // Mode Name, 例如 @"kCFRunLoopDefaultMode"
    CFMutableSetRef _sources0;    // Set
    CFMutableSetRef _sources1;    // Set
    CFMutableArrayRef _observers; // Array
    CFMutableArrayRef _timers;    // Array
    ...
};
 
struct __CFRunLoop {
    CFMutableSetRef _commonModes;     // Set
    CFMutableSetRef _commonModeItems; // Set<Source/Observer/Timer>
    CFRunLoopModeRef _currentMode;    // Current Runloop Mode
    CFMutableSetRef _modes;           // Set
    ...
};
```

这里有个概念叫 “CommonModes”：一个 Mode 可以将自己标记为”Common”属性（通过将其 ModeName 添加到 RunLoop 的 “commonModes” 中）。每当 RunLoop 的内容发生变化时，RunLoop 都会自动将 _commonModeItems 里的 Source/Observer/Timer 同步到具有 “Common” 标记的所有Mode里。


#### RunLoop 的内部逻辑

![](media/15569334563848/15569367935945.png)

#### RunLoop 的底层实现

OSX/iOS 的系统架构
![](media/15569334563848/15569370615730.png)

 Darwin 核心架构
 ![](media/15569334563848/15569370801638.png)

在硬件层上面的三个组成部分：Mach、BSD、IOKit (还包括一些上面没标注的内容)，共同组成了 XNU 内核。XNU 内核的内环被称作 Mach，其作为一个微内核，仅提供了诸如处理器调度、IPC (进程间通信)等非常少量的基础服务。BSD 层可以看作围绕 Mach 层的一个外环，其提供了诸如进程管理、文件系统和网络等功能。IOKit 层是为设备驱动提供了一个面向对象(C++)的一个框架。

Mach 只能通过消息传递的方式实现对象间的通信。”消息”是 Mach 中最基础的概念，消息在两个端口 (port) 之间传递，这就是 Mach 的 IPC (进程间通信) 的核心。

Mach 的消息定义是在 <mach/message.h> 头文件的
    
```
typedef struct {
  mach_msg_header_t header;
  mach_msg_body_t body;
} mach_msg_base_t;
 
typedef struct {
  mach_msg_bits_t msgh_bits;
  mach_msg_size_t msgh_size;
  mach_port_t msgh_remote_port;
  mach_port_t msgh_local_port;
  mach_port_name_t msgh_voucher_port;
  mach_msg_id_t msgh_id;
} mach_msg_header_t;
```

一条 Mach 消息实际上就是一个二进制数据包 (BLOB)，其头部定义了当前端口 local_port 和目标端口 remote_port，发送和接受消息是通过同一个 API 进行的，其 option 标记了消息传递的方向：

```
mach_msg_return_t mach_msg(
			mach_msg_header_t *msg,
			mach_msg_option_t option,
			mach_msg_size_t send_size,
			mach_msg_size_t rcv_size,
			mach_port_name_t rcv_name,
			mach_msg_timeout_t timeout,
			mach_port_name_t notify);
```

Mach通过 mach_msg() 完成消息发送、接收。

RunLoop 的核心就是一个 mach_msg() ，RunLoop 调用这个函数去接收消息，如果没有别人发送 port 消息过来，内核会将线程置于等待状态。

#### Runloop与AutoReleasePool

App启动后，苹果在主线程 RunLoop 里注册了两个 Observer，其回调都是 _wrapRunLoopWithAutoreleasePoolHandler()。

第一个 Observer 监视的事件是 Entry(即将进入Loop)，其回调内会调用 _objc_autoreleasePoolPush() 创建自动释放池。其 order 是-2147483647，优先级最高，保证创建释放池发生在其他所有回调之前。

第二个 Observer 监视了两个事件： BeforeWaiting(准备进入休眠) 时调用_objc_autoreleasePoolPop() 和 _objc_autoreleasePoolPush() 释放旧的池并创建新池；Exit(即将退出Loop) 时调用 _objc_autoreleasePoolPop() 来释放自动释放池。这个 Observer 的 order 是 2147483647，优先级最低，保证其释放池子发生在其他所有回调之后。

在主线程执行的代码，通常是写在诸如事件回调、Timer回调内的。这些回调会被 RunLoop 创建好的 AutoreleasePool 环绕着，所以不会出现内存泄漏，开发者也不必显示创建 Pool 了。

#### Runloop 与事件响应

苹果注册了一个 Source1 (基于 mach port 的) 用来接收系统事件，其回调函数为 __IOHIDEventSystemClientQueueCallback()。

当一个硬件事件(触摸/锁屏/摇晃等)发生后，首先由 IOKit.framework 生成一个 IOHIDEvent 事件并由 SpringBoard 接收。SpringBoard 只接收按键(锁屏/静音等)，触摸，加速，接近传感器等几种 Event，随后用 mach port 转发给需要的App进程。随后苹果注册的那个 Source1 就会触发回调，并调用 _UIApplicationHandleEventQueue() 进行应用内部的分发。

_UIApplicationHandleEventQueue() 会把 IOHIDEvent 处理并包装成 UIEvent 进行处理或分发，其中包括识别 UIGesture/处理屏幕旋转/发送给 UIWindow 等。通常事件比如 UIButton 点击、touchesBegin/Move/End/Cancel 事件都是在这个回调中完成的。

当_UIApplicationHandleEventQueue() 识别了一个手势时，其首先会调用 Cancel 将当前的 touchesBegin/Move/End 系列回调打断。随后系统将对应的 UIGestureRecognizer 标记为待处理。

苹果注册了一个 Observer 监测 BeforeWaiting (Loop即将进入休眠) 事件，这个Observer的回调函数是 _UIGestureRecognizerUpdateObserver()，其内部会获取所有刚被标记为待处理的 GestureRecognizer，并执行GestureRecognizer的回调。

当有 UIGestureRecognizer 的变化(创建/销毁/状态改变)时，这个回调都会进行相应处理。

#### 定时器

NSTimer 其实就是 CFRunLoopTimerRef，他们之间是 toll-free bridged 的。一个 NSTimer 注册到 RunLoop 后，RunLoop 会为其重复的时间点注册好事件。例如 10:00, 10:10, 10:20 这几个时间点。RunLoop为了节省资源，并不会在非常准确的时间点回调这个Timer。Timer 有个属性叫做 Tolerance (宽容度)，标示了当时间点到后，容许有多少最大误差。

如果某个时间点被错过了，例如执行了一个很长的任务，则那个时间点的回调也会跳过去，不会延后执行。就比如等公交，如果 10:10 时我忙着玩手机错过了那个点的公交，那我只能等 10:20 这一趟了。

CADisplayLink 是一个和屏幕刷新率一致的定时器（但实际实现原理更复杂，和 NSTimer 并不一样，其内部实际是操作了一个 Source）。如果在两次屏幕刷新之间执行了一个长任务，那其中就会有一帧被跳过去（和 NSTimer 相似），造成界面卡顿的感觉。


#### NSTimer & Runloop Mode

以 scheduledTimerWithTimeInterval 的方式触发的 timer，在滑动页面上的列表时，timer 会暂停，为什么？该如何解决？

滑动列表时，runloop 的 mode 由原来的 NSDefaultRunLoopMode 模式切换到了 NSEventTrackingRunLoopMode 模式。

解决方法其一是将 timer 加入到 NSRunloopCommonModes 中。其二是将 timer 放到另一个线程中，然后开启另一个线程的 runloop，这样可以保证与主线程互不干扰，而现在主线程正在处理页面滑动。示例代码如下：

```
// 方法1
[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];

// 方法2
dispatch_async(dispatch_get_global_queue(0, 0), ^{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(repeat:) userInfo:nil repeats:true];
    [[NSRunLoop currentRunLoop] run];
});
```

#### 参考

1. 摘抄自 [《深入理解RunLoop》](https://blog.ibireme.com/2015/05/18/runloop/)





### OC

#### @property的作用是什么？

编译器自动添加实例变量和getter/setter方法

#### @property的修饰符有哪些？

nonatomic/atomic readonly/readwrite weak/strong/copy/assign/unsafe_unretain

#### atomic对应的含义，是线程安全的么

atomic保证了getter、setter的原子性(完整性)，不是线程安全的

#### 如果保证访问属性线程安全

创建并发的queue，读写操作操作都在queue中完成，写操作用dispatch_barrier API完成；

#### @property (copy) NSMutableArray *array; 有什么问题？

1. 添加,删除,修改数组会报 unrecognized selector sent to instance，set方法会把array copy成NSArray，NSArray没有增删方法；
2. 默认是atomic，有性能问题；

#### @property、@synthesize、@dynamic

1. @synthesize 的语义是如果你没有手动实现 setter 方法和 getter 方法，那么编译器会自动为你加上这两个方法；
2. @dynamic 告诉编译器：属性的 setter 与 getter 方法由用户自己实现，不自动生成；
3. 如果@synthesize和@dynamic都没写，那么默认的就是@syntheszie var = _var;

#### block和delegate的区别是什么？

block和delegate都是回调的方式，block声明和实现代码集中而delegate分散，所以block相对简单、轻便，delegate易于代码解耦；

block运行成本高，可能需要从栈拷贝到堆，delegate只是保存的对象的引用；

#### KVC

```
// 直接通过Key来取值
- (nullable id)valueForKey:(NSString *)key;  
                        
// 通过Key来设值
- (void)setValue:(nullable id)value forKey:(NSString *)key;          

// 通过KeyPath来取值
- (nullable id)valueForKeyPath:(NSString *)keyPath;                  

// 通过KeyPath来设值
- (void)setValue:(nullable id)value forKeyPath:(NSString *)keyPath;  

//默认返回YES，表示如果没有找到Set<Key>方法的话，会按照_key，_iskey，key，iskey的顺序搜索成员，设置成NO就不这样搜索
+ (BOOL)accessInstanceVariablesDirectly;

//KVC提供属性值确认的API，它可以用来检查set的值是否正确、为不正确的值做一个替换值或者拒绝设置新值并返回错误原因。
- (BOOL)validateValue:(inout id __nullable * __nonnull)ioValue forKey:(NSString *)inKey error:(out NSError **)outError;

//这是集合操作的API，里面还有一系列这样的API，如果属性是一个NSMutableArray，那么可以用这个方法来返回
- (NSMutableArray *)mutableArrayValueForKey:(NSString *)key;

//如果Key不存在，且没有KVC无法搜索到任何和Key有关的字段或者属性，则会调用这个方法，默认是抛出异常
- (nullable id)valueForUndefinedKey:(NSString *)key;

//和上一个方法一样，只不过是设值。
- (void)setValue:(nullable id)value forUndefinedKey:(NSString *)key;

//如果你在SetValue方法时面给Value传nil，则会调用这个方法
- (void)setNilValueForKey:(NSString *)key;

//输入一组key,返回该组key对应的Value，再转成字典返回，用于将Model转到字典。
- (NSDictionary<NSString *, id> *)dictionaryWithValuesForKeys:(NSArray<NSString *> *)keys;
```

1. KVC如果是找到set方法赋值，会触发KVO

#### KVO实现

当你观察一个对象时，一个新的类会被动态创建。这个类继承自该对象的原本的类，并重写了被观察属性的 setter 方法。重写的 setter 方法会负责在调用原 setter 方法之前和之后，通知所有观察对象：值的更改。最后通过 isa 混写（isa-swizzling） 把这个对象的 isa 指针 ( isa 指针告诉 Runtime 系统这个对象的类是什么 ) 指向这个新创建的子类，对象就神奇的变成了新创建的子类的实例。

#### 如何手动触发KVO

实现以下两个方法：

```
willChangeValueForKey
didChangevlueForKey
//  didChangeValueForKey: 内部调用了observeValueForKeyPath:ofObject:change:context:
```

#### nil / Nil / NULL / NSNull

![](media/15567024559404/15569626589005.jpg)



### Swift

#### 类（class）和结构体（struct）有什么区别？

在 Swift 中，类是引用类型，结构体是值类型。值类型在传递和赋值时将进行复制，而引用类型则只会使用引用对象的一个"指向"。

内存中，引用类型诸如类是在堆（heap）上，而值类型诸如结构体是在栈（stack）上进行存储和操作。相比于栈上的操作，堆上的操作更加复杂耗时，所以苹果官方推荐使用结构体，这样可以提高 App 运行的效率。

class 优点：
1. class 可以继承，这样子类可以使用父类的特性和方法；
2. 类型转换可以在 runtime 的时候检查和解释一个实例的类型；
3. 可以用 deinit 来释放资源；
4. 一个类可以被多次引用。

struct 优点：
1. 结构较小，适用于复制操作，相比于一个 class 的实例被多次引用更加安全；
2. 无须担心内存 memory leak 或者多线程冲突问题。

#### Swift 是面向对象还是函数式的编程语言？

Swift 既是面向对象的，又是函数式的编程语言。

说 Swift 是面向对象的语言，是因为 Swift 支持类的封装、继承、和多态，从这点上来看与 Java 这类纯面向对象的语言几乎毫无差别。

说 Swift 是函数式编程语言，是因为 Swift 支持 map, reduce, filter, flatmap 这类去除中间状态、数学函数式的方法，更加强调运算结果而不是中间过程。

#### 在 Swift 中，什么是可选型（optional） ？

在 Swift 中，可选型是为了表达当一个变量值为空的情况。当一个值为空时，它就是 nil。Swift 中无论是引用类型或是值类型的变量，都可以是可选型变量。

Objective-C 中没有明确提出可选型的概念，然而其引用类型却可以为 nil，以此来标识其变量值为空的情况。Swift 将这一理念扩大到值类型，并且明确提出了可选型的概念。

#### 在 Swift 中，什么是泛型（Generics）？

泛型在 Swift 中主要为增加代码的灵活性而生：它可以使得对应的代码满足任意类型的变量或方法。

#### 请说明并比较以下关键词：Open, Public, Internal, File-private, Private

Swift 有五个级别的访问控制权限，从高到底依次为比如 Open, Public, Internal, File-private, Private。

他们遵循的基本原则是：高级别的变量不允许被定义为低级别变量的成员变量。比如一个 private 的 class 中不能含有 public 的 String。反之，低级别的变量却可以定义在高级别的变量中。比如 public 的 class 中可以含有 private 的 Int。

1. Open 具备最高的访问权限。其修饰的类和方法可以在任意 Module 中被访问和重写；它是 Swift 3 中新添加的访问权限。
2. Public 的权限仅次于 Open。与 Open 唯一的区别在于它修饰的对象可以在任意 Module 中被访问，但不能重写。
3. Internal 是默认的权限。它表示只能在当前定义的 Module 中访问和重写，它可以被一个 Module 中的多个文件访问，但不可以被其他的 Module 中被访问。
4. File-private 也是 Swift 3 新添加的权限。其被修饰的对象只能在当前文件中被使用。例如它可以被一个文件中的不同 class，extension，struct 共同使用。
5. Private 是最低的访问权限。它的对象只能在定义的作用域内及其对应的扩展内使用。离开了这个对象，即使是同一个文件中的对象，也无法访问。

#### 请说明并比较以下关键词：strong, weak, unowned

Swift 的内存管理机制与 Objective-C一样为 ARC（Automatic Reference Counting）。它的基本原理是，一个对象在没有任何强引用指向它时，其占用的内存会被回收。反之，只要有任何一个强引用指向该对象，它就会一直存在于内存中。

1. strong 代表着强引用，是默认属性。当一个对象被声明为 strong 时，就表示父层级对该对象有一个强引用的指向。此时该对象的引用计数会增加1。
2. weak 代表着弱引用。当对象被声明为 weak 时，父层级对此对象没有指向，该对象的引用计数不会增加1。它在对象释放后弱引用也随即消失。继续访问该对象，程序会得到 nil，不会崩溃。
3. unowned 与弱引用本质上一样。唯一不同的是，对象在释放后，依然有一个无效的引用指向对象，它不是 Optional 也不指向 nil。如果继续访问该对象，程序就会崩溃。


#### 在 Swift 中，怎样理解是 copy-on-write？

当值类型比如 struct 在复制时，复制的对象和原对象实际上在内存中指向同一个对象。当且仅当复制后的对象进行修改的时候，才会在内存中重新创建一个新的对象。这样设计使得值类型可以多次复制而无需耗费多余内存，只有变化的时候才会增加开销。因此内存的使用更加高效。

#### 什么是属性观察（Property Observer）？

属性观察是指在当前类型内对特定属性进行监视，并作出响应的行为。它是 Swift 的特性，有两种，为 willSet 和 didSet。初始化方法对属性的设定，以及在 willSet 和 didSet 中对属性的再次设定都不会触发属性观察的调用。

#### 结构体中修改成员变量的方法

请问下面代码有什么问题？

```
protocol Pet {
  var name: String { get set }
}

struct MyDog: Pet {
  var name: String

  func changeName(name: String) {
    self.name = name
  }
}
```
应该在 func changeName(name: String) 前加上关键词 mutating，表示该方法将会修改结构体中自己的成员变量。

注意，在设计协议的时候，由于protocol 可以被 class 和 struct 或者 enum 实现，故而要考虑是否用 mutating 来修饰方法。

类（class）中不存在这个问题，因为类可以随意修改自己的成员变量。


#### 用 Swift 实现或（||）操作

这题解法很多，下面给出一种最直接的解法：

```
func ||(left: Bool, right: Bool) –> Bool {
  if left {
    return true
  } else {
    return right
  }
}
```
上面这种解法勉强正确，但是并不高效。或（||）操作的本质是当左边为真的时候，我们无需计算右边。而上面这种事先，是将右边默认值预先准备好，再传入进行操作。当右边值的计算十分复杂时会 造成了性能上的浪费。所以，上面这种做法违反了或（||）操作的本质。正确的实现方法如下：

```
func ||(left: Bool, right: @autoclosure () -> Bool) –> Bool {
  if left {
    return true
  } else {
    return right()
  }
}
```
autoclosure 可以将右边值的计算推迟到判定 left 为 false 的时候，这样就可以避免第一种方法带来的不必要开销了。

#### 实现一个函数。输入是任一整数，输出要返回输入的整数+ 2

这道题看似简单，直接这样写：

```
func addTwo(_ num: Int) -> Int {
    return num + 2
}
```

接下来面试官会说，那假如我要实现 + 4 呢？程序员想了一想，又定义了另一个方法：

```
func addFour(_ num: Int) -> Int {
    return num + 4
}
```

这时面试官会问，假如我要实现返回 + 6, + 8 的操作呢？能不能只定义一次方法呢？正确的写法是利用
Swift 的 Currying 特性：

```
func add(_ num: Int) -> (Int) -> Int {
  return { val in
    return num + val
  }
}
```


```
let addTwo = add(2), addFour = add(4), addSix = add(6), addEight = add(8)
```

Swift 中的柯里化（柯里化） 特性是函数式编程思想的体现。它将接受多个参数的方法进行变形，并用高阶函数的方式进行处理，使整个代码更加灵活。

OC实现

```
typedef void (^AddBlock)(NSInteger value);
- (AddBlock)add:(NSInteger)number {
    return ^(NSInteger value){
        return value+number;
    };
}
```

#### 实现一个函数。求 0 到 100（包括0和100）以内是偶数并且恰好是其他数字平方的数。

这道题十分简单。最简单粗暴的写法如下：

```
func evenSquareNums(from: Int, to: Int) -> [Int] {
  var res = [Int]()

  for num in from...to where num % 2 == 0{
    if (from...to).contains(num * num) {
      res.append(num * num)
    }
  }

  return res
}
```

evenSquareNums(from: 0, to: 100)
上面的写法正确，但不够优雅。首先这个方法完全可以利用泛型进行优化，同时可以在创建 res 数组时加上 reserveCapacity 以保证其性能。其实面对这个题目，最简单直接的写法是用函数式编程的思路，一行既可以解决问题：

```
(0...10).map { $0 * $0 }.filter { $0 % 2 == 0 }
```

Swift 有函数式编程的思想。其中 flatMap, map, reduce, filter 是其代表的方法。本题中考察了 map
和 filter 的组合使用。相比于一般的 for 循环，这样的写法要更加得简洁漂亮。


#### 说说 Swift 为什么将 String，Array，Dictionary设计成值类型？

要解答这个问题，就要和 Objective-C 中相同的数据结构设计进行比较。Objective-C 中，字符串，数组，字典，皆被设计为引用类型。

值类型相比引用类型，最大的优势在于内存使用的高效。值类型在栈上操作，引用类型在堆上操作。栈上的操作仅仅是单个指针的上下移动，而堆上的操作则牵涉到合并、移位、重新链接等。也就是说 Swift 这样设计，大幅减少了堆上的内存分配和回收的次数。同时 copy-on-write 又将值传递和复制的开销降到了最低。

String，Array，Dictionary 设计成值类型，也是为了线程安全考虑。通过 Swift 的 let 设置，使得这些数据达到了真正意义上的“不变”，它也从根本上解决了多线程中内存访问和操作顺序的问题。


#### 用 Swift 将协议（protocol）中的部分方法设计成可选（optional），该怎样实现？

@optional和 @required 是 Objective-C 中特有的关键字。

在 Swift 中，默认所有方法在协议中都是必须实现的。而且，协议里方法不可以直接定义 optional。先给出两种解决方案：

在协议和方法前都加上 @objc 关键字，然后再在方法前加上 optional 关键字。该方法实际上是把协议转化为 Objective-C 的方式然后进行可选定义。示例如下：

```
@objc protocol SomeProtocol {
  func requiredFunc()
  @objc optional func optionalFunc()
}
```

用扩展（extension）来规定可选方法。在 Swift 中，协议扩展（protocol extension）可以定义部分方法的默认实现，这样这些方法在实际调用中就是可选实现的了。示例如下：

```
protocol SomeProtocol {
  func requiredFunc()
  func optionalFunc()
}

extension SomeProtocol {
  func optionalFunc() {
    print(“Dumb Implementation”)
  }
}

Class SomeClass: SomeProtocol {
  func requiredFunc() {
    print(“Only need to implement the required”)
  }
}
```

#### 在 Swift 和 Objective-C 的混编项目中，如何在 Swift 文件中调用 Objective-C 文件中已经定义的方法？如何在 Objective-C 文件中调用 Swift 文件中定义的方法？

1. 在 Swift 中，若要使用 Objective-C 代码，可以在 ProjectName-Bridging-Header.h 里添加 Objective-C 的头文件名称，这样在 Swift 文件中即可调用相应的 Objective-C 代码。一般情况 Xcode 会在 Swift 项目中第一次创建 Objective-C 文件时自动创建 ProjectName-Bridging-Header.h 文件。
2. Objective-C 中若要调用 Swift 代码，可以导入 Swift 生成的头函数 ProjectName-Swift.h 来实现。
3. 在 Swift 文件中，若要规定固定的方法或属性暴露给 Objective-C 使用，可以在方法或属性前加上 @objc 来声明。如果该类是 NSObject 子类，那么 Swift 会在非 private 的方法或属性前自动加上 @objc 。

#### 试比较 Swift 和 Objective-C 中的初始化方法（init）有什么异同？

一言以蔽之，在 Swift 中的初始化方法更加严格和准确。

Objective-C 中，初始化方法无法保证所有成员变量都完成初始化；编译器对属性设置并无警告，但是实际操作中会出现初始化不完全的问题；初始化方法与普通方法并无实际差别，可以多次调用。

在 Swift 中，初始化方法必须保证所有非 optional 的成员变量都完成初始化。同时新增 convenience 和 required 两个修饰初始化方法的关键词。convenience 只是提供一种方便的初始化方法，必须通过调用同一个类中 designated 初始化方法来完成。required 是强制子类重写父类中所修饰的初始化方法。

#### 试比较 Swift 和 Objective-C 中的协议（Protocol）有什么异同？

相同点在于，Swift 和 Objective-C 中的 Protocol 都可以被用作代理。Objective-C 中的 Protocol 类似于 Java 中的 Interface，实际开发中主要用于适配器模式（Adapter Pattern）。

不同点在于，Swift 的 Protocol 还可以对接口进行抽象，例如 Sequence，配合拓展（extension）、泛型、关联类型等可以实现面向协议的编程，从而大大提高整个代码的灵活性。同时 Swift 的 Protocol 还可以用于值类型，如结构体和枚举。

### 其他

#### LLDB 中 p 和 po 有什么区别？

1. p 是 expr – 的缩写。它做的工作是把接收到的参数在当前环境下编译，然后打印出对应的值。
2. po 是 expr –o– 的缩写。它所做的操作与 p 相同。如果接收到的参数是个指针，它会调用对象的 description 方法，并进行打印；如果是个 core foundation 对象，那么会调用 CFShow 方法，并进行打印。如果这两个方法都调用失败，po 打印出和 p 相同的内容。
3. 总的来说 po 相对于 p 会打印出更多内容。一般工作中，用 p 即可，因为 p 操作较少效率较高。

#### Xcode 中的 Runtime issues 和 Buildtime issues 指什么？

1. Buildtime issues 有三类：编译器识别出的警告（Warning），错误（Error），以及静态分析（Static Code Analysis）。前两者无须赘述，静态分析错误一般有这几类：未初始化的变量，未使用数据，API 使用错误。
2. Runtime issues 有三类：线程问题，UI 布局和渲染问题，以及内存问题。

#### 如何用 Xcode 检测代码中的循环引用？

1. 使用 Xcode 中的 Memory Debug Graph，Xcode 会自动检测内存相关的 memory runtime issue。
2. 用 Instruments 里面的 Leak 工具。进入页面后发现 Leak Checks 中出现内存泄漏时，我们可以将导航栏切换到 call tree 模式下，强烈建议在 Display Settings 中勾选 Separate by Thread 和 Hide System Libraries 两个选项，这样可以隐藏掉系统和应用本身的调用路径，帮助我们更方便的找出 retain cycle 位置。

#### 该怎样解决 EXC_BAD_ACCESS？

EXC_BAD_ACCESS 主要原因是访问了某些已经释放的对象，或者访问了它们已经释放的成员变量或方法。解决方法主要有以下几种：

1. 设置全局断点快速定位 bug 所在，这种方法效果一般；
2. 重写 object 的 respondsToSelector 方法，这种方法效果一般且要在每个 class 上进行定点排查，不推荐；
3. 使用 Zombie 和 Address Sanitizer，可以在绝大多数情况下定位问题代码;

#### UIView 和 CALayer 有什么区别？

1. UIView 和 CALayer 都是 UI 操作的对象。两者都是 NSObject 的子类，发生在 UIView 上的操作本质上也发生在对应的 CALayer 上。
2. UIView 是 CALayer 用于交互的抽象。UIView 是 UIResponder 的子类（ UIResponder 是 NSObject 的子类），提供了很多 CALayer 所没有的交互上的接口，主要负责处理用户触发的种种操作。
3. CALayer 在图像和动画渲染上性能更好。这是因为 UIView 有冗余的交互接口，而且相比 CALayer 还有层级之分。CALayer 在无需处理交互时进行渲染可以节省大量时间。
4. CALayer 内部并没有属性，当调用属性方法时，它内部是通过运行时 resolveInstanceMethod 为对象临时添加一个方法，并把对应属性值保存到内部的一个 Dictionary 里，同时还会通知 delegate、创建动画等等，非常消耗资源。UIView 的关于显示相关的属性（比如 frame/bounds/transform）等实际上都是 CALayer 属性映射来的。

#### 请说明并比较以下关键词：Frame, Bounds, Center

1. Frame 是指当前视图（View）相对于父视图的平面坐标系统中的位置和大小。
2. Bounds 是指当前视图相对于自己的平面坐标系统中的位置和大小。
3. Center 是一个 CGPoint，指当前视图在父视图的平面坐标系统中最中间位置点 。

#### CALayer anchorPoint & postion

1、position是layer中的anchorPoint在superLayer中的位置坐标。 
2、互不影响原则：单独修改position与anchorPoint中任何一个属性都不影响另一个属性。 
3、frame、position与anchorPoint有以下关系.
    
position.x =  frame.origin.x + anchorPoint.x * bounds.size.width；
position.y =  frame.origin.y + anchorPoint.y * bounds.size.height； 

     

#### IBOutlet连出来的视图属性为什么可以被设置成weak?

视图的私有数组强引用了属性；


#### 请说明并比较以下方法：layoutIfNeeded, layoutSubviews, setNeedsLayout

1. layoutIfNeeded 方法一旦调用，主线程会立即强制重新布局，它从当前视图开始，一直到完成所有子视图的布局。
2. layoutSubviews 是用来自定义视图尺寸调整的。它是系统自动调用的，开发者不能手动调用。我们能做的就是重写该方法，让系统在尺寸调整时能按照希望的效果去进行布局。这个方法主要在屏幕旋转、滑动或触摸界面、子视图修改时被触发。
3. setNeedsLayout 与 layoutIfNeeded 相似，唯一不同的就是它不会立刻强制视图重新布局，而是在下一个布局周期才会触发更新。它主要用在多个 view 布局先后更新的场景下。例如我们要在两个布局不停变化的点之间连一条线，这个线的布局就可以调用 setNeedsLayout 方法。

#### 请说明并比较以下关键词：Safe Area, SafeAreaLayoutGuide, SafeAreaInsets

由于 iPhone X 全新的刘海设计，iOS 11 中引入了安全区域（Safe Area）这套概念。

1. Safe Area 是指应用合理显示内容的区域。它不包括 status bar, navigation bar, tab bar , tool bar 等。iPhone X 中一般是指扣除了顶部的 status bar（高度为44）和底部的 home indicator 区域（高度为34），这样应用的内容不会被刘海挡住或是影响底部手势操作。
2. SafeAreaLayoutGuide 是指 SafeArea 的区域范围和限制 。在布局设置中，我们可以分别取得它的上下左右 4 个边界的位置进行相应布局处理。
3. SafeAreaInsets 限定了 SafeArea 区域与整个屏幕之间的布局关系。一般我们用上下左右 4 个值来获取 SafeArea 与屏幕边缘之间的距离。

#### iOS 中实现动画的方式有几种？

最主要的实现动画方式有 3 种，UIView Animation、CALayer Animation、UIViewPropertyAnimator。

1. UIView Animation 可以实现基于 UIView 的简单动画。它是 CALayer Animation 的封装，主要可以实现移动、旋转、缩放、变色等基本操作。其基本函数为+ animateWithDuration:animations:，其中持续时间（duration）为基本参数，block 中对 UIView 属性的调整就是动画结束后的最终效果。除此之外他还有关键帧动画和两个 view 转化等接口。它实现的动画无法回撤、暂停、与手势交互。
2. CALayer Animation 是更在底层 CALayer 上的动画接口。除了 UIView Animation 可以实现的效果。它可以修改更多的属性以实现各种复杂的动画效果。其实现的动画可以回撤、暂停、与手势交互。
3. UIViewPropertyAnimator 是 iOS 10 引进的处理交互式动画的接口。它也是基于 UIView 实现，可以实现所有的 UIView Animation 效果。它最大的优点在于 timing function 以及与手势配合的交互式动画设置相比 CALayer Animation 十分简便，可以说是为交互而生的动画接口。

#### UICollectionView 中的 Supplementary Views 和 Decoration Views 分别指什么？

Cells，Supplementary Views，Decoration Views 共同构成了整个 UICollectionView 的视图。

1. Cells 是最基本的、必须用户自己实现并配置的。而 Supplementary Views 和 Decoration Views 有默认实现，主要是用来美化 UICollecctionView 的。
2. Supplementary Views 是补充视图。一般用来设置每个 Seciton 的 Header View 或者Footer View，用来标记 Section 的 View。
3. Decoration Views 是装饰视图。完全跟数据没有关系的视图，负责给 cell 或者 supplementary Views 添加辅助视图用的，例如给单个 section 或整个 UICollectionView 的背景（background）设置就属于 Decoration Views。

Supplementary Views 的布局一般可以在 UICollectionViewFlowLayout 中实现完成。如果要定制化实现 Supplementary Views 和 Decoration Views，那就要实现 UICollectionViewLayout 抽象类中。

#### 常见的设计模式

1. 单例模式（Singleton）：单例模式保证对于一个特有的类，只有一个公共的实例存在。它一般与懒加载一起出现，只有被需要时才会创建。
2. 观察者模式（Observer）：定义对象间的一种一对多依赖关系，使得每当一个对象状态发生改变时，其相关依赖对象皆得到通知并被自动更新。在 iOS 中的典型实现是 NotificationCenter 和 KVO。
3. 装饰模式（Decorator）：可以在不修改原代码的基础上进行拓展。比如：Category、Extension、Delegate。
4. 适配器模式（Adapter）：将一个类的接口转化为另一个类的接口，使得原本互不兼容的类可以通过接口一起工作。

#### MVC、MVP、MVVM

MVC![](media/15567061968407/15568500479050.png)


MVP![](media/15567061968407/15568500834076.png)


MVVM![](media/15567061968407/15568501135293.png)


MVC、MVP、MVVM 三种架构皆由模型层（M - Model），视图层（V - View），中间层（C/P/VM - Controller/Presenter/View Model）构成。我们的比较就先从局部开始——分别比较这三个部分，再到整体差异。

1. 模型层几乎相同。三种架构的模型理论来说都是数据来源，没有什么不同。
2. 视图层理论上都设计为被动，但是实际上略有不同。实际开发中 MVC 中视图层与中间层高度耦合，几乎所有的操作都统一由 ViewController 包办。 但理论上来说，MVC 是希望视图层就是单纯的 UIView，或者 UIViewController 只负责 UI 更新交互，不涉及业务逻辑和模型更新。 MVP 和 MVVM 在实际开发中视图层实现了 MVC 理论期望，即与中间层严格分离。 MVP 中视图层是完全被动，单纯的把交互和更新传递给中间层；而 MVVM 中视图层并不是完全被动——它会监视中间层的变化，一旦产生变化，则视图层也会相应变化。
3. 中间层的设计是三种架构的核心的差异。逻辑上讲，中间层的作用就是连接视图层和模型层。它处理交互、接受通知、完成数据更新。 MVC 的中间层 Controller 持有视图和模型，主要起到一个组装和连接的作用，通过传递参数和实例变量来直接完成所有操作。 MVP 的中间层 Presenter 持有模型，在更新模型上与 MVC 的 Controller 角色一样。但它不拥有视图，视图拥有中间层，中间层的工作流程是：从视图层接收交互传递->响应->向视图层传递响应指令->视图进行更新。全部操作必须手动书写代码完成。 MVVM 的中间层 View Model 持有模型，在更新模型上与前两者相同。它完全独立于视图，视图拥有中间层，通过绑定属性，自动进行更新。全部操作由响应式逻辑框架自动完成。
4. MVC 耦合度很高，代码分配最不合理，维护和扩展成本最高。但因为无需层级传递，所以代码总量最少，适合初学者理解和应用。
5. MVP 和 MVVM 相似，耦合度和代码分配都比较合理，较易实现高测试覆盖率。MVP 的缺点是视图层需要将所有的交互传递给中间层，且要手动实现响应和更新，所以总代码量远超 MVVM。MVVM 在响应和更新上通过响应式框架自动操作，大大精简了代码量；但是需要引入第三方响应式框架，同时因为属性观察环环相扣，调用栈很大，debug 起来尤为痛苦。
6. MVC，MVP，MVVM 这三种结构都是以视图为驱动的架构，三种皆为用户交互和视图更新为主要服务目标。它们一个共同的缺点是没有涉及界面之间的跳转——即路由的设计。

#### 要给一个 UIButton 增加一个点击后抖动的效果，该怎样实现？

1. 实现一个自定义的 UIButton 类，在其中添加点击抖动效果的方法（shake 方法）;在自定义的类中添加 shake 方法扩展性不好。如果 shake 方法被用在其他地方，又要在其他类中再添加一遍 shake 方法，这样代码复用性差。
2. 写一个 UIButton 或者 UIView 的拓展（extension），然后在其中增加 shake 方法;在 extension 中实现虽然解决了代码复用性问题，但是可读性比较差。团队开发中并不是所有人都知道这个 extension 中存在 shake 方法，同时随着功能的扩展，extension 中新增的方法会层出不穷，它们很难归类管理。
3. 定义一个 protocol，然后在协议扩展（protocol extension）中添加 shake 方法;用协议定义解决了复用性、可读性、维护性三个难题。协议的命名（例如 Shakeable）直接可以确定其实现的 UIButton 拥有相应 shake 功能；通过协议扩展，可以针对不同类实现特定的方法，可维护性也大大提高；因为协议扩展通用于所有实现对象，所以代码复用性也很高。（OC中也可以用__attribute__((constructor)) 添加协议的默认实现）

#### 什么是 iOS 中的 App Thinning？

App Thinning ，中文翻译为“应用瘦身”，指的是 App store 和操作系统在安装 iOS 或者 watchOS 的 App 的时候通过一些列的优化，尽可能减少安装包的大小，使得 App 以最节省资源的、最合适的大小被安装到你的设备上。其中有三种类型：slicing, bitcode, and on-demand resources。

1. Slicing 指的是根据不同的设备，App 对应产生相应的版本。如 iPad 版本只包含 iPad 版本的图片资源和布局代码，iPhone 版本则类似。此时下载 App 的时候，只需要下载对应版本的 App 即可。
2. Bitcode 是一个 llvm 编译 App 时生成的中间形式。上传或下载新版本的 App 时，苹果会针对 Bitcode 包含的信息进行针对性地添加或筛选，而不是完整地提交或下载一个新的 App。在 iOS 中它是可选的，在 WatchOS 中 Bitcode 则是必须的。
3. On-Demand Resources 是只提供部分的 App 内容，只要足以满足其基本运行即可。比如某些游戏 App，一开始下载之后只能运行最初的内容，而不是全部的内容。如果玩家有兴趣继续探索，App Store 就会解锁后续内容，将其下载更新到游戏中。

#### imageNamed vs imageWithContentsOfFile

1. imageWithContentsOfFile获取不到Asset里面的图片；
2. imageWithContentsOfFile没有缓存，imageNamed有缓存；
3. 通过 imageNamed 创建 UIImage 时，系统实际上只是在 Bundle 内查找到文件名，然后把这个文件名放到 UIImage 里返回，并没有进行实际的文件读取和解码。当 UIImage 第一次显示到屏幕上时，其内部的解码方法才会被调用，同时解码结果会保存到一个全局缓存去。在图片解码后，App 第一次退到后台和收到内存警告时，该图片的缓存才会被清空，其他情况下缓存会一直存在。


#### 卡顿

![](media/15569482884690/15569487318063.png)

在 VSync 信号到来后，系统图形服务会通过 CADisplayLink 等机制通知 App，App 主线程开始在 CPU 中计算显示内容，比如视图的创建、布局计算、图片解码、文本绘制等。随后 CPU 会将计算好的内容提交到 GPU 去，由 GPU 进行变换、合成、渲染。随后 GPU 会把渲染结果提交到帧缓冲区去，等待下一次 VSync 信号到来时显示到屏幕上。由于垂直同步的机制，如果在一个 VSync 时间内，CPU 或者 GPU 没有完成内容提交，则那一帧就会被丢弃，等待下一次机会再显示，而这时显示屏会保留之前的内容不变。这就是界面卡顿的原因。


#### 离屏渲染（Off-Screen Rendering）

GPU在当前屏幕缓冲区以外开辟一个缓冲区进行渲染操作。离屏渲染需要开辟一个新的缓存区，还要切换上下文，所以性能消耗高。

当使用圆角，阴影，遮罩的时候，图层属性的混合体被指定为在未预合成之前(下一个VSync信号开始前)不能直接在屏幕中绘制，所以就需要屏幕外渲染。 

shouldRasterize = YES在其它属性触发离屏渲染的同时,会将光栅化后的内容缓存起来,如果对应的layer或者 sublayers没有发生改变,在下一帧的时候可以直接复用,从而减少渲染的频率.

#### 事件响应连

##### Hit-Test（事件传递）

在 iOS 中，只有 UIResponder 及其子类才可以响应事件（CALayer继承自NSObject）；UIResponder提供以下方法：

```
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;
// For Apple Pencil
- (void)touchesEstimatedPropertiesUpdated:(NSSet<UITouch *> *)touches NS_AVAILABLE_IOS(9_1);
```

用户触摸屏幕产生事件，系统将事件交给 UIApplication 管理分发，UIApplication 将事件分发给 KeyWindow，然后再寻找出一个最合适的响应者来响应这个事件。主要靠以下两个方法：

```
//返回最合适的 View 来响应事件
- (nullable UIView *)hitTest:(CGPoint)point withEvent:(nullable UIEvent *)event;  
// 判断当前的触摸点是否在 View 中
- (BOOL)pointInside:(CGPoint)point withEvent:(nullable UIEvent *)event; 
```

找到响应者后会调用响应者的 touch 函数进行事件处理。

##### Response Chain（事件响应）
![](media/15567061968407/15569521997507.jpg)


#### iOS 渲染

iOS渲染视图的核心是 Core Animation。从底层到上层依此是 GPU->(OpenGL、Core Graphic) -> Core Animation -> UIKit。

##### 渲染流程：

App内部：

1. 布局：在这个阶段，程序设置 View/Layer 的层级信息，设置 layer 的属性，如 frame，background color 等等。
2. 创建 backing image：在这个阶段程序会创建 layer 的 backing image，无论是通过 setContents 将一个 image 传給 layer，还是通过 drawRect：或 drawLayer:inContext：来画出来的。所以 drawRect：等函数是在这个阶段被调用的。
3. 准备：在这个阶段，Core Animation 框架准备要渲染的 layer 的各种属性数据，以及要做的动画的参数，准备传递給 render server。同时在这个阶段也会解压要渲染的 image。
4. 提交：在这个阶段，Core Animation 打包 layer 的信息以及需要做的动画的参数，通过 IPC（inter-Process Communication）传递給 render server。

App外部：

当这些数据到达 render server 后，会被反序列化成 render tree。然后 render server 会做下面的两件事：

1. 根据 layer 的各种属性（如果是动画的，会计算动画 layer 的属性的中间值），用 OpenGL 准备渲染。
2. 渲染这些可视的 layer 到屏幕。

#### Exception Code：0x8badf00d

主线程卡住的时间太长，被系统watchdog强杀。

#### 怎么去优化一个卡顿的界面

1. 卡顿监测，一般有两种办法：
    1. 主线程卡顿监控。通过子线程监测主线程的 runLoop，判断两个状态区域之间的耗时是否达到一定阈值。（多次连续小卡顿和单次长时间卡顿）
    2. FPS监控。要保持流畅的UI交互，App 刷新率应该当努力保持在 60fps。监控实现原理比较简单，通过记录两次刷新时间间隔，就可以计算出当前的 FPS。
2. 卡顿解决
    1. 优化业务流程；
    2. 控制线程数量；
    3. 预处理和延时加载；（UITableView的优化）
    4. 缓存；

#### UITableView 优化

1. Cell复用；
2. 缓存Cell高度，Runloop空闲时提前计算；
3. 减少Cell层级，减少Subview移除、添加操作；
4. 优化ImageView圆角处理（Core Graphics绘制圆角图片、叠加一个部分透明的图片）；
5. 减少Cell内部AutoLayout的调整
6. 预加载下一页内容

#### SDWebImage下载了图片后为什么要解码

下载的图片是经过编码压缩后的图片数据，并不是可以直接显示的位图。SDWebImage为了提高效率，在子线程中解码图片。


#### UIView的生命周期

1. loadView
2. viewDidLoad
3. viewDidUnLoad
4. viewWillAppear
5. viewWillLayoutSubViews
6. viewDidLayoutSubViews
7. viewDidAppear
8. viewWillDisappear
9. viewDidDisappear
10. didReceiveMemoryWarning


#### #include #import @class @import

1. \#include相当于拷贝头文件中的内容
2. \#import避免了重复include带来的编译错误
3. @class告诉编译器有这样的类，避免循环引用
4. @import会自动链接Framework


#### iOS 开发中本地消息通知的流程是怎样的？

1. 注册。通过调用 requestAuthorization 这个方法，通知中心会向用户发送通知许可请求。在弹出的 Alert 中点击同意，即可完成注册。
2. 创建。首先设置信息内容 UNMutableNotificationContent 和触发机制 UNNotificationTrigger ；然后用这两个值来创建 UNNotificationRequest；最后将 request 加入到当前通知中心 UNUserNotificationCenter.current() 中。
3. 推送。这一步就是系统或者远程服务器推送通知。伴随着一声清脆的响声（或自定义的声音），通知对应的 UI 显示到手机界面的过程。
4. 响应。当用户看到通知后，点击进去会有相应的响应选项。设置响应选项是 UNNotificationAction 和 UNNotificationCategory。

#### iOS 开发中远程消息推送的原理是怎样的？

1. App 向 iOS 系统申请远程消息推送权限。这与本地消息推送的注册是一样的；
2. iOS 系统向 APNs(Apple Push Notification Service) 服务器请求手机的 device token，并告诉 App，允许接受推送的通知；
3. App 将手机的 device token 传给 App 对应的服务器端；
4. 远程消息由 App 对应的服务器端产生，它会先经过 APNs；
5. APNs 将远程通知推送给响应手机。


#### iOS 开发中数据持久化的方案

数据持久化就是将数据保存在硬盘中，这样无论是断网还是重启，我们都可以访问到之前保存的数据。iOS 开发中有以下几种方案：

1. plist。它是一个 XML 文件，会将某些固定类型的数据存放于其中，读写分别通过 contentsOfFile 和 writeToFile 来完成。一般用于保存 App 的基本参数。
2. Preference。它通过 UserDefaults 来完成 key-value 配对保存。如果需要立刻保存，需要调用 synchronize 方法。它会将相关数据保存在同一个 plist 文件下，同样是用于保存 App 的基本参数信息。
3. NSKeyedArchiver。遵循 NSCoding 协议的对象就就可以实现序列化。NSCoding 有两个必须要实现的方法，即父类的归档 initWithCoder 和解档 encodeWithCoder 方法。存储数据通过 NSKeyedArchiver 的工厂方法 archiveRootObject:toFile: 来实现；读取数据通过 NSKeyedUnarchiver 的工厂方法 unarchiveObjectwithFile:来实现。相比于前两者， NSKeyedArchiver 可以任意指定存储的位置和文件名。
4. CoreData。前面几种方法，都是覆盖存储。修改数据要读取整个文件，修改后再覆盖写入，十分不适合大量数据存储。CoreData 就是苹果官方推出的大规模数据持久化的方案。它的基本逻辑类似于 SQL 数据库，每个表为 Entity，然后我们可以添加、读取、修改、删除对象实例。它可以像 SQL 一样提供模糊搜索、过滤搜索、表关联等各种复杂操作。尽管功能强大，它的缺点是学习曲线高，操作复杂。

### TCP/IP

#### OSI模型

1. 物理层（Physical Layer）在局部局域网上传送数据帧（data frame），它负责管理计算机通信设备和网络媒体之间的互通。包括了针脚、电压、线缆规范、集线器、中继器、网卡、主机接口卡等。
2. 数据链路层（Data Link Layer）负责网络寻址、错误侦测和改错。当表头和表尾被加至数据包时，会形成帧。数据链表头（DLH）是包含了物理地址和错误侦测及改错的方法。数据链表尾（DLT）是一串指示数据包末端的字符串。例如以太网、无线局域网（Wi-Fi）和通用分组无线服务（GPRS）等。
3. 网络层（Network Layer）决定数据的路径选择和转寄，将网络表头（NH）加至数据包，以形成分组。网络表头包含了网络数据。例如:互联网协议（IP）等。
4. 传输层（Transport Layer）把传输表头（TH）加至数据以形成数据包。传输表头包含了所使用的协议等发送信息。例如:传输控制协议（TCP）等。
5. 会话层（Session Layer）负责在数据传输中设置和维护计算机网络中两台计算机之间的通信连接。
6. 表达层（Presentation Layer）把数据转换为能与接收者的系统格式兼容并适合传输的格式。
7. 应用层（Application Layer）提供为应用软件而设的接口，以设置与另一应用软件之间的通信。例如: HTTP，HTTPS，FTP，TELNET，SSH，SMTP，POP3.HTML.等。

![](media/15570395488088/15570602621364.jpg)



#### TCP 

一个TCP连接由一个4元组构成，分别是两个IP地址和两个端口号。

##### 名词解释

1. TCP：Transmission Control Protocol 传输控制协议；
2. SYN：Synchronize，在建立连接时使用，布尔值；
3. ACK：Acknowledge，确认收到了消息，布尔值；
4. FIN：Finish，在断开连接时使用，布尔值；
5. SEQ NO：Sequence Number，服务端和客户端都会维护自己的SEQ NO，表示“已经发送了多少数据”，单位是字节；
6. ACK NO：Acknowledge Number，用来回复确认，对应SEQ NO的数据已经收到；
7. ACK vs ACK NO：ACK是布尔值，用来标识数据报的类型，ACK NO是数值用来确认已经收到的数据；
8. ISN：Initial Sequence Number，建立连接时，SEQ NO 专门的名字；

##### 三次握手

![](media/15570395488088/15570471332553.jpg)

##### 为什么是三次握手

任何一方要向另一方发数据（SYN），都必须收到确认回应（ACK）。

TCP 设计中一个基本设定就是，通过 TCP 连接发送的每一个包，都有一个 Sequence Number。而因为每个包都是有序列号的，所以都能被确认收到这些包。

确认机制是累计的，所以一个对 Sequence Number X 的确认，意味着 X 序列号之前 (不包括 X) 包都是被确认接收到的。

TCP 协议是不限制一个连接被重复使用的。这样就有一个问题：这条连接突然断开重连后，TCP 怎么样识别之前旧链接重发的包？——这就需要独一无二的 ISN（初始序列号）机制。

当一个新连接建立时，ISN生成器会生成一个新的32位的ISN，维护一个时钟，时钟每过4微秒，计数器自增1。这样2^32耗时就是差不多4个半小时（MSL，Max Segment Lifetime），这基本超出了任何数据包在网络中的可能传输时间，所以可以认为这种ISN是独一无二的。

发送方与接收方都会有自己的 ISN （下面的例子中就是 X 与 Y）来做双方互发通信，具体的描述如下：

1. A --> B SYN my sequence number is X
2. A <-- B ACK your sequence number is X 
3. A <-- B SYN my sequence number is Y 
4. A --> B ACK your sequence number is Y

2与3都是B发送给A，因此可以合并在一起，因此成为 three way (or three message) handshake

##### 四次挥手

![](media/15570395488088/15570480181925.jpg)


为什么建立连接是三次握手，而关闭连接却是四次挥手呢？

这是因为服务端在LISTEN状态下，收到建立连接请求的SYN报文后，把ACK和SYN放在一个报文里发送给客户端。而关闭连接时，当收到对方的FIN报文时，仅仅表示对方不再发送数据了但是还能接收数据，己方是否现在关闭发送数据通道，需要上层应用来决定，因此，己方ACK和FIN一般都会分开发送。

##### TCP Header

![](media/15570395488088/15570507933143.png)

##### TCP状态转换

![](media/15570395488088/15570509353177.jpg)


1. TCP有哪些状态：CLOSED、LISTEN、SYN-SEND、SYN-REVD、ESTABLISED、FIN-WAIT、CLOSE-WAIT、TIME-WAIT；
2. 收到ACK状态变为ESTABLISED，收到FIN状态变为CLOSE-WAIT
3. TIME-WAIT，等待2MSL，作用就是用来重发可能丢失的ACK，防止已失效的连接请求报文段出现在本连接中；

##### TCP流量控制与拥塞控制

如果某个应用进程读取比较缓慢，但是发送方发送的太多、太快，发送的数据就会很容易地使该连接的接收缓存溢出。

TCP为它的应用程序提供了流量控制服务(flow-control service)以消除发送方使接收方缓存溢出的可能性。流量控制因此是一个速度匹配服务，即发送方的发送速率与接收方应用程序的读取速率相匹配。

TCP通过让发送方维护一个称为接收窗口(receive window)的变量(TCP报文段首部的接收窗口字段)来提供流量控制。通俗的讲，接收窗口用于给发送方一个指示－－该接收方还有多少可用的缓存空间。因为TCP是全双工通信，在连接两端的发送方都各自维护了一个接收窗口。

![](media/15570395488088/15570576074384.jpg)

如上图所示RcvBuffer是接收缓存的总大小，buffered data是当前已经缓存了的数据，而free buffer space是当前剩余的缓存空间大小,rwnd的值就是free buffer space。主机B通过把当前的rwnd值放入到它发送给主机A的报文段首部的接收窗口字段中，通知主机A它在该连接的缓存中还有多少可用空间。
而主机A则将自己发往主机B的序号空间中未确认的数据量控制在rwnd值的范围内,这样就可以避免主机A使主机B的接收缓存溢出。

在TCP协议中，分组丢失一般是当网络变得拥塞时由路由器缓存溢出引起的。因此分组重传是作为网络拥塞的征兆来对待，但是却无法处理导致网络拥塞的原因，因为有太多的源想以过高的速率发送数据。一旦网络发生拥塞，分组所经历的时延会变大，分组丢失的可能性会变大，发送端需要重传的分组会变多，这只会导致网络越来越拥塞，形成恶性循环。因此，为了处理网络拥塞，需要一些机制以在面临网络拥塞时遏制发送方。

TCP是通过端到端的方法来解决拥塞控制的，因为IP层不会向端系统提供有关网络拥塞的反馈信息。TCP报文段的丢失被认为是网络拥塞的一个迹象，TCP会相应的减小其窗口长度。

TCP拥塞控制算法包括三部分:慢启动，拥塞避免，快速恢复。

##### TCP粘包

TCP是一个基于字节流的传输服务，"流"意味着TCP所传输的数据是没有边界的。这不同于UDP提供基于消息的传输服务，其传输的数据是有边界的。TCP的发送方无法保证对等方每次接收到的是一个完整的数据包。主机A向主机B发送两个数据包，主机B的接收情况可能是

![](media/15570395488088/15570648120571.png)

产生粘包问题的原因有以下几个：

1. 应用层调用write方法，将应用层的缓冲区中的数据拷贝到套接字的发送缓冲区。而发送缓冲区有一个SO_SNDBUF的限制，如果应用层的缓冲区数据大小大于套接字发送缓冲区的大小，则数据需要进行多次的发送。
2. TCP所传输的报文段有MSS的限制，如果套接字缓冲区的大小大于MSS，也会导致消息的分割发送。
3. 链路层最大发送单元MTU，在IP层会进行数据的分片。

粘包问题的最本质原因在与接收对等方无法分辨消息与消息之间的边界在哪。我们通过使用某种方案给出边界，例如：

1. 发送定长包。如果每个消息的大小都是一样的，那么在接收对等方只要累计接收数据，直到数据等于一个定长的数值就将它作为一个消息。
2. 包尾加上\r\n标记。FTP协议正是这么做的。但问题在于如果数据正文中也含有\r\n，则会误判为消息的边界。
3. 包头加上包体长度。包头是定长的4个字节，说明了包体的长度。接收对等方先接收包体长度，依据包体长度来接收包体。


#### TCP vs UDP

1. TCP协议是有连接的，有连接的意思是开始传输实际数据之前TCP的客户端和服务器端必须通过三次握手建立连接，会话结束之后也要结束连接。而UDP是无连接的
2. TCP协议保证数据按序发送，按序到达，提供超时重传来保证可靠性，但是UDP不保证按序到达，甚至不保证到达，只是努力交付，即便是按序发送的序列，也不保证按序送到。
3. TCP协议所需资源多，TCP首部需20个字节（不算可选项），UDP首部字段只需8个字节。
4. TCP有流量控制和拥塞控制，UDP没有，网络拥堵不会影响发送端的发送速率
5. TCP是一对一的连接，而UDP则可以支持一对一，多对多，一对多的通信。
6. TCP面向的是字节流的服务，UDP面向的是报文的服务。

运行在TCP协议上的协议：HTTP、HTTPS、FTP、POP3、SMTP、TELNET、SSH。

运行在UDP协议上的协议：BOOTP（Boot Protocol，启动协议），应用于无盘设备。NTP（Network Time Protocol，网络时间协议），用于网络同步。DHCP（Dynamic Host Configuration Protocol，动态主机配置协议），动态配置IP地址。

#### HTTP 首部

1. 通用首部字段：Cache-Control、Connection、Date、Trailer、Transfer-Encoding
2. 请求首部字段：Accept、Accept-Encoding、􏱱􏱲􏱳􏱴􏱵􏱐􏱶􏱷􏱸􏱳􏱶􏱵􏱹Authorization、From、Host、User-Agent
3. 响应首部字段：Age、Server、Location
4. 实体首部字段：Allow、Content-Encoding、Content-Length、Content-Type、Expires、Set-Cookie；

#### HTTP

1. HTTP 协议构建于 TCP/IP 协议之上，是一个应用层协议，默认端口号是 80；
2. HTTP 是无连接无状态的；
3. HTTP 四种基本方法：GET、POST、PUT、DELETE；
    1. GET 用于信息获取，而且应该是安全和幂等的，GET获取信息不产生副作用，幂等的意味着对同一 URL 的多个请求应该返回同样的结果；
    2. GET 可提交的数据量受到URL长度的限制；
    3. POST 提交的数据必须在 body 部分

#### 常见状态码

1. 200 请求成功
2. 301 Moved Permanently 请求永久重定向
3. 302 Moved Temporarily 请求临时重定向
4. 304 Not Modified 文件未修改，可以直接使用缓存的文件。
5. 400 Bad Request 由于客户端请求有语法错误，不能被服务器所理解。
6. 401 Unauthorized 请求未经授权。这个状态代码必须和WWW-Authenticate报头域一起使用
7. 403 Forbidden 服务器收到请求，但是拒绝提供服务。服务器通常会在响应正文中给出不提供服务的原因
8. 404 Not Found 请求的资源不存在，例如，输入了错误的URL
9. 500 Internal Server Error 服务器发生不可预期的错误，导致无法完成客户端的请求。
10. 503 Service Unavailable 服务器当前不能够处理客户端的请求，在一段时间之后，服务器可能会恢复正常。
11. 504 网关超时，请求上游服务器超时；


#### Cookie vs Session

1. Cookie会根据响应报文里的一个叫做Set-Cookie的首部字段信息，通知客户端保存Cookie。当下客户端再向服务端发起请求时，客户端会自动在请求报文中加入Cookie值之后发送出去；
2. Set-Cookie的值：logcookie=xxx Cookie名称/值，expires有效期，path 限定Cookie发送范围的文件目录，domain 指定域名，secure HTTPs下可以发送Cookie，HttpOnly 使JavaScript脚本无法获得Cookie；
3. Session是存储在服务器端的，Cookie是存储在客户端的；
4. Session默认保存在Cookie中，禁用Cookie，Session会附在URL中；

#### HTTPS

![](media/15570395488088/15570605442754.jpg)


##### 加密（解决HTTP可以被监听的问题）

对通信内容使用加密算法和秘钥进行加密，然后用加密后的内容进行通信，这样即使通信内容被窃听，没有秘钥(及时加密算法公开)，就无法解密。

1. 对称秘钥加密技术
	* 加密、解密使用同一个密钥；
	* 加密、解密速度快；
	* 如何将同一个密钥交给通信双方是个问题；
	* DES、3-DES、AES、RC5、RC6；
	
2. 非对称(公开)密钥加密技术
	* 加密、解密不使用同一个密钥，分公钥、私钥；
	* 公钥公开，私钥自己保存；
	* 公钥可以解密私钥加密的内容，私钥可以解密公钥加密的内容；
	* 加密、解密速度慢；
	* RSA；

![04-公钥私钥](media/15540975376942/04-%E5%85%AC%E9%92%A5%E7%A7%81%E9%92%A5.png)


3. HTTPS使用混合加密机制
	
	先用非对称密钥加密技术建立通信，间接交换对称密钥，然后用对称密钥加密。下面有详细介绍。

##### 数字签名（解决HTTP可以伪装的问题）

利用数字签名就可以对通信内容进行完整请校验。

数字签名：

![05-数字签名](media/15540975376942/05-%E6%95%B0%E5%AD%97%E7%AD%BE%E5%90%8D.png)



##### 数字证书（解决HTTP可以窃听的问题）

数字证书：

![06-数字签名格式](media/15540975376942/06-%E6%95%B0%E5%AD%97%E7%AD%BE%E5%90%8D%E6%A0%BC%E5%BC%8F.jpg)


擦，写了一堆还是描述不清楚，看图：

![07-签发数字证书](media/15540975376942/07-%E7%AD%BE%E5%8F%91%E6%95%B0%E5%AD%97%E8%AF%81%E4%B9%A6.png)



##### HTTPS通信流程


![08-HTTPS通信流程](media/15540975376942/08-HTTPS%E9%80%9A%E4%BF%A1%E6%B5%81%E7%A8%8B.png)



#### Socket

Socket是对TCP/IP协议的封装，Socket提供调用接口，方便使用TCP/IP协议。

![](media/15570395488088/15570635461904.png)


创建Socket流程
![](media/15570395488088/15570636311144.jpg)


#### 序列化与反序列化

1. 序列化： 将数据结构或对象转换成二进制串的过程。
2. 反序列化：将在序列化过程中所生成的二进制串转换成数据结构或者对象的过程

NSKeyedArchiver、NSKeyedUnarchiver，model需要实现NSCoding协议，encode和decode；

序列化协议：

1. XML，格式统一；文件庞大，解析复杂；
2. JSON，格式简单，易于解析；没有XML通用；


#### 请说明并比较以下类：URLSessionTask，URLSessionDataTask，URLSessionUploadTask，URLSessionDownloadTask

1. URLSessionTask 是个抽象类。通过实现它可以实例化任意网络传输任务，诸如请求、上传、下载任务。它的暂停（cancel）、继续（resume）、终止（suspend）方法有默认实现
2. URLSessionDataTask 负责 HTTP GET 请求。它是 URLSessionTask 的具体实现。一般用于从服务器端获取数据，并存放在内存中。
3. URLSessionUploadTask 负责 HTTP Post/Put 请求。它继承了 URLSessionDataTask。一般用于上传数据。
4. URLSessionDownloadTask 负责下载数据。它是 URLSessionTask 的具体实现。它一般将下载的数据保存在一个临时的文件中；在 cancel 后可将数据保存，并之后继续下载。


![](media/15570395488088/15571133676094.png)


### 算法

#### 不用临时变量怎么实现 swap(a, b)

```
a = a+b;
b = a-b;
a = a-b;
```

```
a = a^b;
b = a^b;
a = a^b;
```

#### 二维数组中的查找

在一个二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。

例如下面的二维数组就是每行、每列都递增排序。如果在这个数组中查找数字7，则返回true；如果查找数字5，由于数组不含有该数字，则返回false。

![](media/15571270550470/15571287894950.jpg)

从右上角开始查找，如果大于7，排除这一列，如果小于7，排除这一行。

```
BOOL find(int *matrix, int rows, int columns, int number) {
    BOOL found = NO;
    if(matrix != NULL && rows > 0 && columns > 0) {
        int row = 0;
        int column = columns - 1;
        while (row<rows && column >= 0) {
            if(matrix[row][column] == number) {
                found = YES;
                break;
            } else if (matrix[row][column] > number) {
                column--;
            } else {
                row++;
            }
        }
    }
    return found;
}
```

#### 排序

[十大经典排序算法](https://www.cnblogs.com/onepixel/p/7674659.html) 

##### 冒泡排序

```
function bubbleSort(arr) {
    var len = arr.length;
    for (var i = 0; i < len - 1; i++) {
        for (var j = 0; j < len - 1 - i; j++) {
            if (arr[j] > arr[j+1]) {        // 相邻元素两两对比
                var temp = arr[j+1];        // 元素交换
                arr[j+1] = arr[j];
                arr[j] = temp;
            }
        }
    }
    return arr;
}
```

##### 选择排序

```
function selectionSort(arr) {
    var len = arr.length;
    var minIndex, temp;
    for (var i = 0; i < len - 1; i++) {
        minIndex = i;
        for (var j = i + 1; j < len; j++) {
            if (arr[j] < arr[minIndex]) {     // 寻找最小的数
                minIndex = j;                 // 将最小数的索引保存
            }
        }
        temp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = temp;
    }
    return arr;
} 
```

##### 插入排序

```
function insertionSort(arr) {
    var len = arr.length;
    var preIndex, current;
    for (var i = 1; i < len; i++) {
        preIndex = i - 1;
        current = arr[i];
        while (preIndex >= 0 && arr[preIndex] > current) {
            arr[preIndex + 1] = arr[preIndex];
            preIndex--;
        }
        arr[preIndex + 1] = current;
    }
    return arr;
}
```

##### 快速排序

```
function quickSort(arr, left, right) {
    var len = arr.length,
        partitionIndex,
        left = typeof left != 'number' ? 0 : left,
        right = typeof right != 'number' ? len - 1 : right;
 
    if (left < right) {
        partitionIndex = partition(arr, left, right);
        quickSort(arr, left, partitionIndex-1);
        quickSort(arr, partitionIndex+1, right);
    }
    return arr;
}
 
function partition(arr, left ,right) {     // 分区操作
    var pivot = left,                      // 设定基准值（pivot）
        index = pivot + 1;
    for (var i = index; i <= right; i++) {
        if (arr[i] < arr[pivot]) {
            swap(arr, i, index);
            index++;
        }       
    }
    swap(arr, pivot, index - 1);
    return index-1;
}
 
function swap(arr, i, j) {
    var temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}
```

##### 堆排序

1. 将无序数组构造成二叉树，对比二叉树子节点和父节点，确保父节点大于子节点，这样找出堆顶就是最大值；
2. 将最大的数字剥离二叉树，二叉树最后一个元素作为堆顶，重复第一步，找出二叉树最大值
3. 重复第二步；

#### 从尾到头打印链表

1. 遍历链表，将链表push到栈中，然后逐个pop栈；
2. 没有到链表尾部时递归调用，链表长时函数调用层级深；
   ```
   void printReverse(ListNode *pHead) {
        if(pHead != NULL) {
            if(pHead->m_pNext != NULL) {
                printReverse(pHead->m_pNext);
            }
            printf("%d\t",pHead->m_nValue);
        }
   }
   ```

#### 树的遍历方式

![](media/15571270550470/15571329918265.jpg)

#### 重构二叉树

输入某二叉树的前序遍历和中序遍历的结果，请重建出该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。例如输入前序遍历序列{1,2,4,7,3,5,6,8}和中序遍历序列{4,7,2,1,5,3,8,6}：

![](media/15571270550470/15571350527431.jpg)

#### 两个栈实现一个队列

![](media/15571270550470/15571356284532.jpg)

#### 两个队列模拟一个栈

![](media/15571270550470/15571357612941.jpg)

#### 旋转数组的最小数字

把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。输入一个递增排序的数组的一个旋转，输出旋转数组的最小元素。例如数组{3,4,5,1,2}为{1,2,3,4,5}的一个旋转，该数组的最小值为1。

![](media/15571270550470/15571361565366.jpg)

#### 斐波那契数列

![](media/15571270550470/15571366727873.jpg)

#### 二进制中1的个数

![](media/15571270550470/15571368261259.jpg)


