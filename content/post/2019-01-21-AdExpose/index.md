---
title: 广告位资源曝光统计
date: 2019-05-20
tags: ["iOS", "广告"]
---

公司要拓展广告业务，与某广告平台合作，在 App 首页信息流中展示广告，并统计广告的曝光、点击行为。大致流程如下：请求首页接口的同时，向广告平台请求广告信息，附加一些用户、设备信息，广告平台返回定制化的广告，在信息流中以图文的形式展示出来。当广告曝光以及用户点击时，需要调用此广告对应的曝光、点击链接，把曝光、点击信息上报给广告平台。

<!--more-->

## 理论探索

用户点击行为上报给广告平台比较简单，不再赘述。下面主要讨论如何检测广告曝光，我们定义广告位从不可见状态转变为可见状态为有效曝光。检测的最小颗粒度是 UIView 即可，UIView 是否可见取决于以下条件：

1. hidden 属性为 NO；
2. alpha 大于 0.01；
3. 存在 superView 且在 superView 的可见区域；

接下来我们为 UIView 添加 Category，增加两个 Associated Object，一个布尔值决定 UIView 是否要检测曝光，另一个是曝光之后的回调 Block。另外在 willMoveWindow 的时候通知下面的 Manager 开始检测。

然后创建一个单例 Manager，来管理哪些 UIView 需要曝光检测，以及在主线程对应的 Runloop 即将休眠（以防卡顿）的时候更新 UIView 的可见状态。

## 具体实现

创建 UIView+Expose Category，.h和.m文件如下：

.h 文件

```
@interface UIView (Expose)

@property (nonatomic, assign) BOOL bqi_ShouldDetectExpose;  // default is NO
@property (nonatomic, copy) void (^bqi_DidExposedBlock)(void);  // called after every expose

- (BOOL)isVisible;  // 当前状态 view 是否可见

@end
```
.m 文件

```
+ (void)load {
    // 替换willMoveToWindow方法
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalSelector = @selector(willMoveToWindow:);
        SEL swizzledSelector = @selector(bqi_willMoveToWindow:);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
        
        method_exchangeImplementations(originalMethod, swizzledMethod);
    });
}

- (void)bqi_willMoveToWindow:(UIWindow *)window {
    [self bqi_willMoveToWindow:window];
    if (self.bqi_ShouldDetectExpose && self.bqi_DidExposedBlock) {
        // Manager 管理哪些 UIView 需要检测曝光
        [[BQIExposeManager sharedInstance] addExposeObserveOfView:self];
    }
}

#pragma mark - Visible

// 判断当前状态是否是可见
- (BOOL)isVisible {
    if (self.hidden || self.alpha <= 0.01 || !self.superview) {
        return NO;
    }
    return [self isVisible:self inView:self.superview];
}

- (BOOL)isVisible:(UIView *)subView inView:(UIView *)view{
    if (subView == view) {
        return YES;
    }
    if ([view isKindOfClass:NSClassFromString(@"UITableViewWrapperView")]) {
        view = view.superview;
    }
    CGRect rect = [view convertRect:subView.frame fromView:view];
    if (CGRectIntersectsRect(rect, view.bounds)) {
        return [self isVisible:view inView:view.superview];
    }
    return NO;
}

#pragma mark - Associated Object

- (BOOL)bqi_ShouldDetectExpose {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void (^)(void))bqi_DidExposedBlock {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setBqi_ShouldDetectExpose:(BOOL)bqi_ShouldDetectExpose {
    objc_setAssociatedObject(self, @selector(bqi_ShouldDetectExpose), @(bqi_ShouldDetectExpose), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setBqi_DidExposedBlock:(void (^)(void))bqi_DidExposedBlock {
    objc_setAssociatedObject(self, @selector(bqi_DidExposedBlock), bqi_DidExposedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
```
判断 UIView 是否可见时需要对 UITableViewWrapperView 特殊处理，iOS 11 以下的系统，cell 的 superView 是 UITableViewWrapperView 而不是 UITableView。

创建 BQIExposeManager，管理需要检测曝光的 UIView，监听 Runloop 状态。

.h 文件

```
@interface BQIExposeManager : NSObject

+ (instancetype)sharedInstance;

// 监测UIView曝光
- (void)addExposeObserveOfView:(UIView *)view;

// 移除监测UIView曝光
- (void)removeExposeObserveOfView:(UIView *)view;

@end
```

.m 文件

```
@interface BQIExposeManager () {
    CFRunLoopObserverRef observer;
}

// 使用HashTable可以避免添加重复的UIView，弱引用UIView，
// UIView dealloc 之后 HashTable 也自动移除此 UIView
@property (nonatomic, strong) NSHashTable <UIView *> *observedViews;
// 上次 Runloop 周期中 UIView 的可见状态，
// 上次不可见，此次可见才算曝光
@property (nonatomic, strong) NSMutableDictionary <NSString *, NSNumber *> *lastRunloopExposeStatus;

@end

@implementation BQIExposeManager

+ (instancetype)sharedInstance {
    static BQIExposeManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[BQIExposeManager alloc] init];
        [manager customInit];
    });
    return manager;
}

- (void)customInit {
    self.observedViews = [NSHashTable weakObjectsHashTable];
    self.lastRunloopExposeStatus = [NSMutableDictionary dictionary];
    
    [self addRunLoopObserver];
    
    // App 切到前台继续监听Runloop状态
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      [self addRunLoopObserver];
    }];
    // App 切到后台不再监听Runloop状态
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      [self removeRunloopObserver];
                                                  }];
}

- (void)addRunLoopObserver {
    if (observer != NULL) {
        [self removeRunloopObserver];
    }
    // 监控线程对应的Runloop即将休眠
    __weak typeof(self) weakSelf = self;
    observer = CFRunLoopObserverCreateWithHandler(kCFAllocatorDefault, kCFRunLoopBeforeWaiting, YES, 0, ^(CFRunLoopObserverRef observer, CFRunLoopActivity activity) {
        if (activity == kCFRunLoopBeforeWaiting) {
            [weakSelf refreshObservedViewsExposeStatus];
        }
    });
    CFRunLoopAddObserver(CFRunLoopGetMain(), observer, kCFRunLoopCommonModes);
}

- (void)removeRunloopObserver {
    if (observer != NULL) {
        CFRunLoopObserverInvalidate(observer);
        CFRelease(observer);
        observer = NULL;
    }
}

- (void)addExposeObserveOfView:(UIView *)view {
    if (!view || ![view isKindOfClass:[UIView class]] ||
        !view.bqi_ShouldDetectExpose || !view.bqi_DidExposedBlock) {
        return ;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateView:view isExposed:NO];
        [self.observedViews addObject:view];
    });
}

- (void)removeExposeObserveOfView:(UIView *)view {
    if (!view || ![view isKindOfClass:[UIView class]]) {
        return ;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeViewExposeStatus:view];
        [self.observedViews removeObject:view];
    });
}

- (void)refreshObservedViewsExposeStatus {
    for (UIView *view in self.observedViews.allObjects) {
        if (view.isVisible) {
            // 调用曝光Block
            if (![self isExposedLastRunloop:view] && view.bqi_DidExposedBlock) {
                view.bqi_DidExposedBlock();
            }
            [self updateView:view isExposed:YES];
        } else {
            [self updateView:view isExposed:NO];
        }
    }
}

- (BOOL)isExposedLastRunloop:(UIView *)view {
    if (!view || ![view isKindOfClass:[UIView class]]) {
        return NO;
    }
    NSString *key = [NSString stringWithFormat:@"%p",view];
    return [[self.lastRunloopExposeStatus valueForKey:key] boolValue];
}

- (void)updateView:(UIView *)view isExposed:(BOOL)isExposed {
    if (!view || ![view isKindOfClass:[UIView class]]) {
        return ;
    }
    NSString *key = [NSString stringWithFormat:@"%p",view];
    [self.lastRunloopExposeStatus setValue:@(isExposed) forKey:key];
}

- (void)removeViewExposeStatus:(UIView *)view {
    if (!view || ![view isKindOfClass:[UIView class]]) {
        return ;
    }
    NSString *key = [NSString stringWithFormat:@"%p",view];
    [self.lastRunloopExposeStatus removeObjectForKey:key];
}
```

## 参考

1. [深入理解Runloop](https://blog.ibireme.com/2015/05/18/runloop/)
3. [婚礼纪 iOS 曝光统计实践](https://mp.weixin.qq.com/s/QQJxGleNylECxPF4whLMgA)

